(function($, Drupal, window, document, undefined) {


    var s = {};

    s.toggleItem = function(e) {

        var $this = $(e.currentTarget),
            $row = $this.parents('tr'),
            $rows = $row.nextAll();

        n = $('td:eq(0) .indentation', $row).length;
        hidden = $row.hasClass('closed');

        $row.toggleClass('closed');
        $rows.each(function(a, b) {

            var $row = $(b),
                i = $('td:eq(0) .indentation', $row).length;

            if (i > n)
                if (hidden) $row.removeClass('hidden');
                else $row.addClass('hidden');

            else return false;


        });


    };


    s.addToggles = function() {

        s.$rows.each(function(a, b) {

            var $this = $('td:eq(0)', b);
            $('<a class="toggle">').click(s.toggleItem).appendTo($this);

        });

    };

    s.toggleAll = function() {

        s.$rows.each(function(a, b) {

            var $this = $(b),
                n = $('td:eq(0) .indentation', $this).length,
                n1 = $('td:eq(0) .indentation', s.$rows[a + 1]).length;
            if (n1 > n || $this.hasClass('closed')) $('a.toggle', $this).trigger('click');

        });


    };

    s.init = function() {

        s.$table = $('table#menu-overview');
        s.$rows = $('tr', s.$table);
        s.addToggles();

        $('<a class="toggle all">').click(s.toggleAll).appendTo('table#menu-overview th:eq(0)')


    }



    $(document).ready(function() {
        $(window).load(function() {

            //s.init();

        });
    });





})(jQuery, Drupal, this, this.document);
