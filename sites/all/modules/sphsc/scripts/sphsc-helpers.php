<?php

// Random helper functions for Drush  
//


function title_case($title) {

	$title = str_replace(array('"', '_', '.html',), ' ', $title); 
	$title = explode(' ', ucwords($title)); 

	$conjunctions = array(
		'of', 'a', 'the', 'and', 'an', 'or', 'nor', 'but', 'is', 'if', 'then', 'else', 'when', 'at', 'from', 'by', 'on', 'off', 'for', 'in', 'out', 'over', 'to', 'into', 'with', 'eg', 'e.g.', 'ie', 'i.e', 'de', 'el', 
 	);
	$abbreviations = array(
		'and' => '&', 'phd' => 'PhD', 'faq' => 'FAQ', '.html' => '', 'coreslp' => 'CORESLP',  
		);
	$delimiters = array(
		'-', '\'', 
		); 


	foreach ($title as $k => $v) {

		$match = array_search(strtolower($v), $conjunctions); 
		$v = $match === false ? $v : $conjunctions[$match]; 

		$match = array_key_exists(strtolower($v), $abbreviations); 
		$v = $match === false ? $v : $abbreviations[strtolower($v)]; 

		foreach($delimiters as $d => $dv) {
			if(strpos($v, $dv) !== false) {
				$v = implode($dv, array_map('ucfirst', explode($dv, $v)));

			}
		}
		
		$title[$k] = $v; 
	}

	return implode(' ', $title); 
}




function get_partial_string($string, $n = 80) {

	$i = substr(trim($string), 0, $n);
	return substr($i, 0, strrpos($i, ' '));

	preg_match("/(?:\w+(?:\W+|$)){0,$n}/is", $string, $a);
	return empty($a[0]) ? $string : $a[0];

}



function get_page($url) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_REFERER, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	$result = curl_exec($ch);
	curl_close($ch);
	return $result;
}


function save_image_to_local($image_url, $save_dir = '/sites/default/files/') {

	$contents = get_page($image_url);

	$u = parse_url($image_url);
	$save_as = '/www/sphsc/sites/'. $save_dir . '/'. basename($u['path']); 

	$f = fopen($save_as, "w");
	fwrite($f, $contents);
	return fclose($f);

}



