#!/usr/bin/env drush
<?php 
// http://drupal.stackexchange.com/questions/9837/how-to-execute-php-script-using-drush

//
// This example demonstrates how to write a drush
// "shebang" script. These scripts start with the
// line "#!/usr/bin/env drush" or "#!/full/path/to/drush".
//
// See `drush topic docs-scripts` for more information.
//
drush_print("Hello world!");
drush_print();
drush_print("The arguments to this command were:");

//
// If called with --everything, use drush_get_arguments
// to print the commandline arguments. Note that this
// call will include 'php-script' (the drush command)
// and the path to this script.
//
if (drush_get_option('everything')) {
 drush_print(" " . implode("\n ", drush_get_arguments()));
}}}
//
// If --everything is not included, then use
// drush_shift to pull off the arguments one at
// a time. drush_shift only returns the user
// commandline arguments, and does not include
// the drush command or the path to this script.
//
else {
 while ($arg = drush_shift()) {
  drush_print(' ' . $arg);
 }}}
}}}

drush_print();



$menu_name = 'main-menu'; 
$tree = menu_tree(); 
print render($tree); 

//print_r(menu_load_links($menu_name)); 
$data = menu_tree_page_data($menu_name);
print_r($data);

/******
select 

	repeat ('*', ROUND ( 
				(LENGTH(u.alias) - LENGTH(REPLACE(u.alias, "/",""))) / LENGTH("/")    
		)-1) as depth, 

	concat( 
		 substring(
			replace( 
				replace(
					replace(title, ' & ', ' and '), 
          'Quick Links', concat('Quick Links - ', alias)), 
				'Not Found', concat('Not Found - ', alias)
        )
			, 1, 250)
      ,
		' {"url":"', u.source,
		'","options":{"attributes":{"title":"',
    u.alias, 
    '"}'
	)
* - n.nid, n.title, u.* 
from url_alias as u, node as n 
where n.nid = replace(u.source, 'node/', '') and n.created > 1440182126
group by n.nid
order by alias;




****/ 
/*****

Academic Partners
* PLACEHOLDER - audiology {"url":"node/721","options":{"attributes":{"title":"academic-programs/audiology"}}}
* Not Found - academic-programs/audiology-WICHE-WRGP-tuition-rate.html (node/741) {"url":"node/741","options":{"attributes":{"title":"academic-programs/audiology-WICHE-WRGP-tuition-rate.html"}}}
** Academic and Student Services Eagleson Hall {"url":"node/720","options":{"attributes":{"title":"academic-programs/audiology/academic-and-student-services3.html"}}}
** Doctor of Audiology Program How to Apply {"url":"node/724","options":{"attributes":{"title":"academic-programs/audiology/audiology-apply.html"}}}
** Doctor of Audiology Program Areas of Interest and Elective Courses {"url":"node/725","options":{"attributes":{"title":"academic-programs/audiology/audiology-areas-interest.html"}}}
** Doctor of Audiology Program ASHA Certification Requirements {"url":"node/726","options":{"attributes":{"title":"academic-programs/audiology/audiology-asha-certification.html"}}}
** Doctor of Audiology/Doctor of Philosophy Combined Degree Program {"url":"node/727","options":{"attributes":{"title":"academic-programs/audiology/audiology-audphd-program.html"}}}
** Doctor of Audiology Program Clinical Rotations {"url":"node/728","options":{"attributes":{"title":"academic-programs/audiology/audiology-clinical-rotations.html"}}}
** Contact or Visit Us {"url":"node/729","options":{"attributes":{"title":"academic-programs/audiology/audiology-contact.html"}}}
** Doctor of Audiology Program Curriculum {"url":"node/730","options":{"attributes":{"title":"academic-programs/audiology/audiology-curriculum.html"}}}
** Doctor of Audiology Program Entrance Requirements {"url":"node/731","options":{"attributes":{"title":"academic-programs/audiology/audiology-entrance-requirements.html"}}}
** Doctor of Audiology Program FAQ {"url":"node/732","options":{"attributes":{"title":"academic-programs/audiology/audiology-faqs.html"}}}
** Doctor of Audiology Program Financial Assistance {"url":"node/733","options":{"attributes":{"title":"academic-programs/audiology/audiology-financial.html"}}}
** Doctor of Audiology Program Student Outcome Data {"url":"node/734","options":{"attributes":{"title":"academic-programs/audiology/audiology-program-statistics.html"}}}
** Doctor of Audiology Program Research Experience {"url":"node/735","options":{"attributes":{"title":"academic-programs/audiology/audiology-research-experience.html"}}}
** Resources and Links {"url":"node/736","options":{"attributes":{"title":"academic-programs/audiology/audiology-resources.html"}}}
** Doctor of Audiology Program Satisfactory Progress {"url":"node/737","options":{"attributes":{"title":"academic-programs/audiology/audiology-satisfactory-progress.html"}}}
** Doctor of Audiology Program Tuition and Fees {"url":"node/738","options":{"attributes":{"title":"academic-programs/audiology/audiology-tuition-fees.html"}}}
** Doctor of Audiology Program WICHE / WRGP Tuition Rate {"url":"node/723","options":{"attributes":{"title":"academic-programs/audiology/audiology-WICHE-WRGP-tuition-rate.html"}}}
** Doctor of Audiology Program Overview {"url":"node/722","options":{"attributes":{"title":"academic-programs/audiology/audiology.html"}}}
** PLACEHOLDER - depts.washington {"url":"node/1150","options":{"attributes":{"title":"academic-programs/audiology/depts.washington.edu"}}}
*** PLACEHOLDER - sphsc {"url":"node/1151","options":{"attributes":{"title":"academic-programs/audiology/depts.washington.edu/sphsc"}}}
**** PLACEHOLDER - academicprograms {"url":"node/1152","options":{"attributes":{"title":"academic-programs/audiology/depts.washington.edu/sphsc/academic-programs"}}}
***** PLACEHOLDER - docs {"url":"node/1155","options":{"attributes":{"title":"academic-programs/audiology/depts.washington.edu/sphsc/academic-programs/docs"}}}
****** PLACEHOLDER - slp both {"url":"node/1156","options":{"attributes":{"title":"academic-programs/audiology/depts.washington.edu/sphsc/academic-programs/docs/slp-both"}}}
******* Not Found - academic-programs/audiology/depts.washington.edu/sphsc/academic-programs/docs/slp-both/SPHSC-Essential-Functions-FINAL.html {"url":"node/1154","options":{"attributes":{"title":"academic-programs/audiology/depts.washington.edu/sphsc/academic-programs/docs/slp-both/SPHSC-Essential-Functions-FINAL.html"}}}
***** PLACEHOLDER - non-degree-enrollment {"url":"node/1153","options":{"attributes":{"title":"academic-programs/audiology/depts.washington.edu/sphsc/academic-programs/non-degree-enrollment"}}}
****** Not Found - academic-programs/audiology/depts.washington.edu/sphsc/academic-programs/non-degree-enrollment/non-degree-enrollment.html {"url":"node/1149","options":{"attributes":{"title":"academic-programs/audiology/depts.washington.edu/sphsc/academic-programs/non-degree-enrollment/non-degree-enrollment.html"}}}
** Not Found - academic-programs/audiology/phd-contact.html {"url":"node/739","options":{"attributes":{"title":"academic-programs/audiology/phd-contact.html"}}}
** Not Found - academic-programs/audiology/phd-faculty-research.html {"url":"node/740","options":{"attributes":{"title":"academic-programs/audiology/phd-faculty-research.html"}}}
* Bachelor of Science in Speech and Hearing Sciences Undergraduate Major Overview {"url":"node/742","options":{"attributes":{"title":"academic-programs/index-2.html"}}}
* Bachelor of Science in Speech and Hearing Sciences Undergraduate Major Overview {"url":"node/719","options":{"attributes":{"title":"academic-programs/index.html"}}}
* PLACEHOLDER - medical-speech-language-pathology {"url":"node/744","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology"}}}
** Academic and Student Services Eagleson Hall {"url":"node/743","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology/academic-and-student-services5.html"}}}
** Not Found - academic-programs/medical-speech-language-pathology/audiology-WICHE-WRGP-tuition-rate.html {"url":"node/745","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology/audiology-WICHE-WRGP-tuition-rate.html"}}}
** Master of Science (M.S.) Student Outcome Data {"url":"node/746","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology/medical-master-of-science-program-statistics.html"}}}
** Medical Speech-Language Pathology Program How to Apply {"url":"node/747","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology/medical-speech-language-pathology-apply.html"}}}
** Medical Speech-Language Pathology Program ASHA Certification Requirements {"url":"node/748","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology/medical-speech-language-pathology-asha.html"}}}
** Medical Speech-Language Pathology Program Clinical Practica and Internships {"url":"node/749","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology/medical-speech-language-pathology-clinical-practica.html"}}}
** Contact or Visit Us {"url":"node/750","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology/medical-speech-language-pathology-contact.html"}}}
** Medical Speech-Language Pathology Program Curriculum {"url":"node/751","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology/medical-speech-language-pathology-curriculum.html"}}}
** Medical Speech-Language Pathology Program Entrance Requirements {"url":"node/752","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology/medical-speech-language-pathology-entrance.html"}}}
** Medical Speech-Language Pathology FAQ {"url":"node/753","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology/medical-speech-language-pathology-faqs.html"}}}
** Medical Speech-Language Pathology Financial Assistance {"url":"node/754","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology/medical-speech-language-pathology-financial.html"}}}
** Medical Speech-Language Pathology Program Overview {"url":"node/755","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology/medical-speech-language-pathology-overview.html"}}}
** Resources and Links {"url":"node/756","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology/medical-speech-language-pathology-resources.html"}}}
** Medical Speech-Language Pathology Program Satisfactory Progress {"url":"node/757","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology/medical-speech-language-pathology-satisfactory-progress.html"}}}
** Medical Speech-Language Pathology Program Tuition and Fees {"url":"node/758","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology/medical-speech-language-pathology-tuition.html"}}}
** Not Found - academic-programs/medical-speech-language-pathology/phd-contact.html {"url":"node/759","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology/phd-contact.html"}}}
** Not Found - academic-programs/medical-speech-language-pathology/phd-faculty-research.html {"url":"node/760","options":{"attributes":{"title":"academic-programs/medical-speech-language-pathology/phd-faculty-research.html"}}}
* PLACEHOLDER - new-graduate-students {"url":"node/762","options":{"attributes":{"title":"academic-programs/new-graduate-students"}}}
** Not Found - academic-programs/new-graduate-students/audiology-WICHE-WRGP-tuition-rate.html {"url":"node/761","options":{"attributes":{"title":"academic-programs/new-graduate-students/audiology-WICHE-WRGP-tuition-rate.html"}}}
** New Graduate Students {"url":"node/763","options":{"attributes":{"title":"academic-programs/new-graduate-students/new-graduate-students.html"}}}
** Not Found - academic-programs/new-graduate-students/phd-contact.html {"url":"node/764","options":{"attributes":{"title":"academic-programs/new-graduate-students/phd-contact.html"}}}
** Not Found - academic-programs/new-graduate-students/phd-faculty-research.html {"url":"node/765","options":{"attributes":{"title":"academic-programs/new-graduate-students/phd-faculty-research.html"}}}
* PLACEHOLDER - non-degree-enrollment {"url":"node/767","options":{"attributes":{"title":"academic-programs/non-degree-enrollment"}}}
** Not Found - academic-programs/non-degree-enrollment/audiology-WICHE-WRGP-tuition-rate.html {"url":"node/766","options":{"attributes":{"title":"academic-programs/non-degree-enrollment/audiology-WICHE-WRGP-tuition-rate.html"}}}
** Non-Degree Enrollment Information {"url":"node/768","options":{"attributes":{"title":"academic-programs/non-degree-enrollment/non-degree-enrollment.html"}}}
** Not Found - academic-programs/non-degree-enrollment/phd-contact.html {"url":"node/769","options":{"attributes":{"title":"academic-programs/non-degree-enrollment/phd-contact.html"}}}
** Not Found - academic-programs/non-degree-enrollment/phd-faculty-research.html {"url":"node/770","options":{"attributes":{"title":"academic-programs/non-degree-enrollment/phd-faculty-research.html"}}}
* PLACEHOLDER - opportunity-accomodations {"url":"node/772","options":{"attributes":{"title":"academic-programs/opportunity-accomodations"}}}
** Not Found - academic-programs/opportunity-accomodations/audiology-WICHE-WRGP-tuition-rate.html {"url":"node/771","options":{"attributes":{"title":"academic-programs/opportunity-accomodations/audiology-WICHE-WRGP-tuition-rate.html"}}}
** Not Found - academic-programs/opportunity-accomodations/phd-contact.html {"url":"node/773","options":{"attributes":{"title":"academic-programs/opportunity-accomodations/phd-contact.html"}}}
** Not Found - academic-programs/opportunity-accomodations/phd-faculty-research.html {"url":"node/774","options":{"attributes":{"title":"academic-programs/opportunity-accomodations/phd-faculty-research.html"}}}
** Equal Opportunity and Accommodations Statement {"url":"node/775","options":{"attributes":{"title":"academic-programs/opportunity-accomodations/statement.html"}}}
* Academic Programs Overview {"url":"node/776","options":{"attributes":{"title":"academic-programs/ovr-overview.html"}}}
* PLACEHOLDER - phd {"url":"node/778","options":{"attributes":{"title":"academic-programs/phd"}}}
* Not Found - academic-programs/phd-contact.html {"url":"node/789","options":{"attributes":{"title":"academic-programs/phd-contact.html"}}}
* Not Found - academic-programs/phd-faculty-research.html {"url":"node/790","options":{"attributes":{"title":"academic-programs/phd-faculty-research.html"}}}
** Academic and Student Services Eagleson Hall {"url":"node/777","options":{"attributes":{"title":"academic-programs/phd/academic-and-student-services6.html"}}}
** Not Found - academic-programs/phd/audiology-WICHE-WRGP-tuition-rate.html {"url":"node/779","options":{"attributes":{"title":"academic-programs/phd/audiology-WICHE-WRGP-tuition-rate.html"}}}
** Doctor of Philosophy Program How to Apply {"url":"node/781","options":{"attributes":{"title":"academic-programs/phd/phd-apply.html"}}}
** Contact or Visit Us {"url":"node/782","options":{"attributes":{"title":"academic-programs/phd/phd-contact.html"}}}
** Doctor of Philosophy Program Curriculum and Research Training {"url":"node/783","options":{"attributes":{"title":"academic-programs/phd/phd-curriculum.html"}}}
** Doctor of Philosophy Program Entrance Requirements {"url":"node/784","options":{"attributes":{"title":"academic-programs/phd/phd-entrance-requirements.html"}}}
** Faculty Research Areas {"url":"node/785","options":{"attributes":{"title":"academic-programs/phd/phd-faculty-research.html"}}}
** Doctor of Philosophy Program FAQ {"url":"node/786","options":{"attributes":{"title":"academic-programs/phd/phd-faqs.html"}}}
** Doctor of Philosophy Financial Support {"url":"node/787","options":{"attributes":{"title":"academic-programs/phd/phd-financial.html"}}}
** Resources and Links       {"url":"node/788","options":{"attributes":{"title":"academic-programs/phd/phd-resources-and-links.html"}}}
** Doctor of Philosophy Program Overview {"url":"node/780","options":{"attributes":{"title":"academic-programs/phd/phd.html"}}}
* PLACEHOLDER - postbaccalaureate {"url":"node/792","options":{"attributes":{"title":"academic-programs/postbaccalaureate"}}}
** Academic and Student Services Eagleson Hall {"url":"node/791","options":{"attributes":{"title":"academic-programs/postbaccalaureate/academic-and-student-services2.html"}}}
** Not Found - academic-programs/postbaccalaureate/audiology-WICHE-WRGP-tuition-rate.html {"url":"node/793","options":{"attributes":{"title":"academic-programs/postbaccalaureate/audiology-WICHE-WRGP-tuition-rate.html"}}}
** Not Found - academic-programs/postbaccalaureate/phd-contact.html {"url":"node/794","options":{"attributes":{"title":"academic-programs/postbaccalaureate/phd-contact.html"}}}
** Not Found - academic-programs/postbaccalaureate/phd-faculty-research.html {"url":"node/795","options":{"attributes":{"title":"academic-programs/postbaccalaureate/phd-faculty-research.html"}}}
** Postbaccalaureate Program How To Apply {"url":"node/796","options":{"attributes":{"title":"academic-programs/postbaccalaureate/postbaccalaureate-apply.html"}}}
** Postbaccalauareate Clinical Observations Information and FAQS {"url":"node/797","options":{"attributes":{"title":"academic-programs/postbaccalaureate/postbaccalaureate-clinical-observations.html"}}}
** Contact or Visit Us {"url":"node/798","options":{"attributes":{"title":"academic-programs/postbaccalaureate/postbaccalaureate-contact.html"}}}
** Postbaccalauareate Program Course Schedules {"url":"node/799","options":{"attributes":{"title":"academic-programs/postbaccalaureate/postbaccalaureate-course-schedule.html"}}}
** Postbaccalaureate Program Curriculum {"url":"node/800","options":{"attributes":{"title":"academic-programs/postbaccalaureate/postbaccalaureate-curriculum.html"}}}
** Postbaccalauareate Program Entrance Requirements {"url":"node/801","options":{"attributes":{"title":"academic-programs/postbaccalaureate/postbaccalaureate-entrance-requirements.html"}}}
** Postbaccalauareate Program Frequently Asked Questions (FAQs) {"url":"node/802","options":{"attributes":{"title":"academic-programs/postbaccalaureate/postbaccalaureate-faqs.html"}}}
** Postbaccalaureate Program Financial Assistance {"url":"node/803","options":{"attributes":{"title":"academic-programs/postbaccalaureate/postbaccalaureate-financial.html"}}}
** Bachelor of Science in Speech and Hearing Sciences Postbaccalaureate Program Overview {"url":"node/804","options":{"attributes":{"title":"academic-programs/postbaccalaureate/postbaccalaureate-overview.html"}}}
** Postbaccalauareate Program Resources and Links {"url":"node/805","options":{"attributes":{"title":"academic-programs/postbaccalaureate/postbaccalaureate-resources.html"}}}
** Postbaccalauareate Program Tuition and Fees {"url":"node/806","options":{"attributes":{"title":"academic-programs/postbaccalaureate/postbaccalaureate-tuition.html"}}}
* PLACEHOLDER - research-training {"url":"node/808","options":{"attributes":{"title":"academic-programs/research-training"}}}
** Not Found - academic-programs/research-training/audiology-WICHE-WRGP-tuition-rate.html {"url":"node/807","options":{"attributes":{"title":"academic-programs/research-training/audiology-WICHE-WRGP-tuition-rate.html"}}}
** Not Found - academic-programs/research-training/lawerner@u.washington.html {"url":"node/809","options":{"attributes":{"title":"academic-programs/research-training/lawerner@u.washington.html"}}}
** Not Found - academic-programs/research-training/phd-contact.html {"url":"node/810","options":{"attributes":{"title":"academic-programs/research-training/phd-contact.html"}}}
** Not Found - academic-programs/research-training/phd-faculty-research.html {"url":"node/811","options":{"attributes":{"title":"academic-programs/research-training/phd-faculty-research.html"}}}
** Research Preceptors {"url":"node/812","options":{"attributes":{"title":"academic-programs/research-training/research-preceptors.html"}}}
** Research Training {"url":"node/813","options":{"attributes":{"title":"academic-programs/research-training/research-training-overview.html"}}}
** Information for Potential Postdoctoral Trainees {"url":"node/814","options":{"attributes":{"title":"academic-programs/research-training/research-training-postdoctoral.html"}}}
** Information for Potential Predoctoral Trainees {"url":"node/815","options":{"attributes":{"title":"academic-programs/research-training/research-training-predoctoral.html"}}}
* PLACEHOLDER - speech-language-pathology {"url":"node/817","options":{"attributes":{"title":"academic-programs/speech-language-pathology"}}}
** Academic and Student Services Eagleson Hall {"url":"node/816","options":{"attributes":{"title":"academic-programs/speech-language-pathology/academic-and-student-services4.html"}}}
** Not Found - academic-programs/speech-language-pathology/audiology-WICHE-WRGP-tuition-rate.html {"url":"node/818","options":{"attributes":{"title":"academic-programs/speech-language-pathology/audiology-WICHE-WRGP-tuition-rate.html"}}}
** Core Speech-Language Pathology Program How to Apply {"url":"node/820","options":{"attributes":{"title":"academic-programs/speech-language-pathology/core-speech-language-pathology-apply.html"}}}
** Core Speech-Language Pathology Program ASHA Certification Requirements {"url":"node/821","options":{"attributes":{"title":"academic-programs/speech-language-pathology/core-speech-language-pathology-asha.html"}}}
** Core Speech-Language Pathology Program Clinical Practica and Internships {"url":"node/822","options":{"attributes":{"title":"academic-programs/speech-language-pathology/core-speech-language-pathology-clinical.html"}}}
** Contact or Visit Us {"url":"node/823","options":{"attributes":{"title":"academic-programs/speech-language-pathology/core-speech-language-pathology-contact.html"}}}
** Core Speech-Language Pathology Program Curriculum {"url":"node/824","options":{"attributes":{"title":"academic-programs/speech-language-pathology/core-speech-language-pathology-curriculum.html"}}}
** Core Speech-Language Pathology Program Emphasis Paths and Electives {"url":"node/825","options":{"attributes":{"title":"academic-programs/speech-language-pathology/core-speech-language-pathology-emphasis.html"}}}
** Core Speech-Language Pathology Program Entrance Requirements {"url":"node/826","options":{"attributes":{"title":"academic-programs/speech-language-pathology/core-speech-language-pathology-entrance.html"}}}
** Core Speech-Language Pathology Program FAQs {"url":"node/827","options":{"attributes":{"title":"academic-programs/speech-language-pathology/core-speech-language-pathology-faqs.html"}}}
** Core Speech-Language Pathology Program Financial Assistance {"url":"node/828","options":{"attributes":{"title":"academic-programs/speech-language-pathology/core-speech-language-pathology-financial.html"}}}
** Core Speech-Language Pathology Program Overview {"url":"node/829","options":{"attributes":{"title":"academic-programs/speech-language-pathology/core-speech-language-pathology-overview.html"}}}
** Resources and Links {"url":"node/830","options":{"attributes":{"title":"academic-programs/speech-language-pathology/core-speech-language-pathology-resources.html"}}}
** Core Speech-Language Pathology Program Satisfactory Progress {"url":"node/831","options":{"attributes":{"title":"academic-programs/speech-language-pathology/core-speech-language-pathology-satisfactory-progress.html"}}}
** Core Speech-Language Pathology Program Tuition and Fees {"url":"node/832","options":{"attributes":{"title":"academic-programs/speech-language-pathology/core-speech-language-pathology-tuition.html"}}}
** Core Speech-Language Pathology Program WICHE / WRGP Tuition Rate {"url":"node/819","options":{"attributes":{"title":"academic-programs/speech-language-pathology/core-speech-language-pathology-WICHE-WRGP.html"}}}
** Master of Science (M.S.) Student Outcome Data {"url":"node/833","options":{"attributes":{"title":"academic-programs/speech-language-pathology/master-of-science-program-statistics-dd.html"}}}
** Not Found - academic-programs/speech-language-pathology/medical-speech-language-pathology-curriculum.html {"url":"node/834","options":{"attributes":{"title":"academic-programs/speech-language-pathology/medical-speech-language-pathology-curriculum.html"}}}
** Not Found - academic-programs/speech-language-pathology/phd-contact.html {"url":"node/835","options":{"attributes":{"title":"academic-programs/speech-language-pathology/phd-contact.html"}}}
** Not Found - academic-programs/speech-language-pathology/phd-faculty-research.html {"url":"node/836","options":{"attributes":{"title":"academic-programs/speech-language-pathology/phd-faculty-research.html"}}}
* PLACEHOLDER - sphsc alumni stories {"url":"node/838","options":{"attributes":{"title":"academic-programs/sphsc-alumni-stories"}}}
** SPHSC Alumni Stories {"url":"node/837","options":{"attributes":{"title":"academic-programs/sphsc-alumni-stories/alumni-stories.html"}}}
** SPHSC Alumni Stories {"url":"node/839","options":{"attributes":{"title":"academic-programs/sphsc-alumni-stories/aud-testimonial.html"}}}
** SPHSC Alumni Stories {"url":"node/840","options":{"attributes":{"title":"academic-programs/sphsc-alumni-stories/aud-testimonial2.html"}}}
** SPHSC Alumni Stories {"url":"node/841","options":{"attributes":{"title":"academic-programs/sphsc-alumni-stories/aud-testimonial3.html"}}}
** Not Found - academic-programs/sphsc-alumni-stories/audiology-WICHE-WRGP-tuition-rate.html {"url":"node/842","options":{"attributes":{"title":"academic-programs/sphsc-alumni-stories/audiology-WICHE-WRGP-tuition-rate.html"}}}
** SPHSC Alumni Stories {"url":"node/843","options":{"attributes":{"title":"academic-programs/sphsc-alumni-stories/coreslp-testimonial.html"}}}
** SPHSC Alumni Stories {"url":"node/844","options":{"attributes":{"title":"academic-programs/sphsc-alumni-stories/coreslp-testimonial2.html"}}}
** SPHSC Alumni Stories {"url":"node/845","options":{"attributes":{"title":"academic-programs/sphsc-alumni-stories/medslp-testimonial.html"}}}
** SPHSC Alumni Stories {"url":"node/846","options":{"attributes":{"title":"academic-programs/sphsc-alumni-stories/medslp-testimonial3.html"}}}
** Not Found - academic-programs/sphsc-alumni-stories/phd-contact.html {"url":"node/847","options":{"attributes":{"title":"academic-programs/sphsc-alumni-stories/phd-contact.html"}}}
** Not Found - academic-programs/sphsc-alumni-stories/phd-faculty-research.html {"url":"node/848","options":{"attributes":{"title":"academic-programs/sphsc-alumni-stories/phd-faculty-research.html"}}}
** SPHSC Alumni Stories {"url":"node/849","options":{"attributes":{"title":"academic-programs/sphsc-alumni-stories/posbac-testimonial.html"}}}
** SPHSC Alumni Stories {"url":"node/850","options":{"attributes":{"title":"academic-programs/sphsc-alumni-stories/posbac-testimonial2.html"}}}
* PLACEHOLDER - undergraduate {"url":"node/852","options":{"attributes":{"title":"academic-programs/undergraduate"}}}
** Academic and Student Services Eagleson Hall {"url":"node/851","options":{"attributes":{"title":"academic-programs/undergraduate/academic-and-student-services.html"}}}
** Not Found - academic-programs/undergraduate/audiology-WICHE-WRGP-tuition-rate.html {"url":"node/853","options":{"attributes":{"title":"academic-programs/undergraduate/audiology-WICHE-WRGP-tuition-rate.html"}}}
** Not Found - academic-programs/undergraduate/phd-contact.html {"url":"node/854","options":{"attributes":{"title":"academic-programs/undergraduate/phd-contact.html"}}}
** Not Found - academic-programs/undergraduate/phd-faculty-research.html {"url":"node/855","options":{"attributes":{"title":"academic-programs/undergraduate/phd-faculty-research.html"}}}
** Undergraduate Transfer Students How to Apply {"url":"node/857","options":{"attributes":{"title":"academic-programs/undergraduate/undergraduate-apply-transfer.html"}}}
** Undergraduate Major How To Apply {"url":"node/856","options":{"attributes":{"title":"academic-programs/undergraduate/undergraduate-apply.html"}}}
** Undergraduate Clinical Observations Information and FAQS {"url":"node/858","options":{"attributes":{"title":"academic-programs/undergraduate/undergraduate-clinical-observations.html"}}}
** Contact or Visit Us {"url":"node/859","options":{"attributes":{"title":"academic-programs/undergraduate/undergraduate-contact.html"}}}
** Undergraduate Major Course Schedules {"url":"node/860","options":{"attributes":{"title":"academic-programs/undergraduate/undergraduate-course-schedules.html"}}}
** Undergraduate Curriculum Overview of Major {"url":"node/861","options":{"attributes":{"title":"academic-programs/undergraduate/undergraduate-curriculum.html"}}}
** Undergraduate Major Frequently Asked Questions (FAQs) {"url":"node/862","options":{"attributes":{"title":"academic-programs/undergraduate/undergraduate-faqs.html"}}}
** Undergraduate Honors Program {"url":"node/863","options":{"attributes":{"title":"academic-programs/undergraduate/undergraduate-honors-program.html"}}}
** Resources and Links {"url":"node/864","options":{"attributes":{"title":"academic-programs/undergraduate/undergraduate-resources.html"}}}
** Undergraduate Transfer Students Frequently Asked Questions (FAQs) {"url":"node/865","options":{"attributes":{"title":"academic-programs/undergraduate/undergraduate-transfer-faqs.html"}}}
** PLACEHOLDER - www.washington {"url":"node/1158","options":{"attributes":{"title":"academic-programs/undergraduate/www.washington.edu"}}}
*** PLACEHOLDER - uaa {"url":"node/1159","options":{"attributes":{"title":"academic-programs/undergraduate/www.washington.edu/uaa"}}}
**** PLACEHOLDER - advising {"url":"node/1160","options":{"attributes":{"title":"academic-programs/undergraduate/www.washington.edu/uaa/advising"}}}
***** PLACEHOLDER - downloads {"url":"node/1161","options":{"attributes":{"title":"academic-programs/undergraduate/www.washington.edu/uaa/advising/downloads"}}}
****** Not Found - academic-programs/undergraduate/www.washington.edu/uaa/advising/downloads/genedreqs.html {"url":"node/1157","options":{"attributes":{"title":"academic-programs/undergraduate/www.washington.edu/uaa/advising/downloads/genedreqs.html"}}}
Alumni
* Academic and Student Services Eagleson Hall {"url":"node/867","options":{"attributes":{"title":"alumni/alumni-academic-student-services.html"}}}
* Alumni Information {"url":"node/868","options":{"attributes":{"title":"alumni/alumni-form.html"}}}
* Alumni Information {"url":"node/869","options":{"attributes":{"title":"alumni/alumni-info.html"}}}
* Alumni {"url":"node/870","options":{"attributes":{"title":"alumni/index-2.html"}}}
* Alumni {"url":"node/866","options":{"attributes":{"title":"alumni/index.html"}}}
* Not Found - art/index.html {"url":"node/871","options":{"attributes":{"title":"art/index.html"}}}
Clinical Services
* Assistive Living Devices {"url":"node/873","options":{"attributes":{"title":"clinical-services/assistive-living-devices.html"}}}
* About Our Clinical Services {"url":"node/874","options":{"attributes":{"title":"clinical-services/clinical-services.html"}}}
* Contact Us {"url":"node/875","options":{"attributes":{"title":"clinical-services/contact-us.html"}}}
* Disability Access {"url":"node/876","options":{"attributes":{"title":"clinical-services/disability-access.html"}}}
* Driving and Parking {"url":"node/877","options":{"attributes":{"title":"clinical-services/driving-parking.html"}}}
* FAQ {"url":"node/878","options":{"attributes":{"title":"clinical-services/faqs.html"}}}
* Audiology Services {"url":"node/879","options":{"attributes":{"title":"clinical-services/hearing-services.html"}}}
* Clinic Overview {"url":"node/880","options":{"attributes":{"title":"clinical-services/index-2.html"}}}
* Clinic Overview {"url":"node/872","options":{"attributes":{"title":"clinical-services/index.html"}}}
* News {"url":"node/881","options":{"attributes":{"title":"clinical-services/news.html"}}}
** Index of /sphsc/clinicalservices/siarc {"url":"node/882","options":{"attributes":{"title":"clinical-services/siarc/index.html"}}}
** Index of /sphsc/clinicalservices/siarc {"url":"node/892","options":{"attributes":{"title":"clinical-services/siarc/index30b5.html"}}}
** Index of /sphsc/clinicalservices/siarc {"url":"node/893","options":{"attributes":{"title":"clinical-services/siarc/index5851.html"}}}
** Index of /sphsc/clinicalservices/siarc {"url":"node/894","options":{"attributes":{"title":"clinical-services/siarc/index72c9.html"}}}
** Index of /sphsc/clinicalservices/siarc {"url":"node/895","options":{"attributes":{"title":"clinical-services/siarc/indexad3f.html"}}}
** Index of /sphsc/clinicalservices/siarc {"url":"node/896","options":{"attributes":{"title":"clinical-services/siarc/indexb70a.html"}}}
** Index of /sphsc/clinicalservices/siarc {"url":"node/897","options":{"attributes":{"title":"clinical-services/siarc/indexbfec.html"}}}
** Index of /sphsc/clinicalservices/siarc {"url":"node/898","options":{"attributes":{"title":"clinical-services/siarc/indexc052.html"}}}
** Index of /sphsc/clinicalservices/siarc {"url":"node/899","options":{"attributes":{"title":"clinical-services/siarc/indexf8a0.html"}}}
** Summer Intensive Auditory Rehabilitation Conference {"url":"node/900","options":{"attributes":{"title":"clinical-services/siarc/siarc-overview.html"}}}
*** Index of /sphsc/clinicalservices/siarc/docs {"url":"node/883","options":{"attributes":{"title":"clinical-services/siarc/docs/index.html"}}}
*** Index of /sphsc/clinicalservices/siarc/docs {"url":"node/884","options":{"attributes":{"title":"clinical-services/siarc/docs/index30b5.html"}}}
*** Index of /sphsc/clinicalservices/siarc/docs {"url":"node/885","options":{"attributes":{"title":"clinical-services/siarc/docs/index5851.html"}}}
*** Index of /sphsc/clinicalservices/siarc/docs {"url":"node/886","options":{"attributes":{"title":"clinical-services/siarc/docs/index72c9.html"}}}
*** Index of /sphsc/clinicalservices/siarc/docs {"url":"node/887","options":{"attributes":{"title":"clinical-services/siarc/docs/indexad3f.html"}}}
*** Index of /sphsc/clinicalservices/siarc/docs {"url":"node/888","options":{"attributes":{"title":"clinical-services/siarc/docs/indexb70a.html"}}}
*** Index of /sphsc/clinicalservices/siarc/docs {"url":"node/889","options":{"attributes":{"title":"clinical-services/siarc/docs/indexbfec.html"}}}
*** Index of /sphsc/clinicalservices/siarc/docs {"url":"node/890","options":{"attributes":{"title":"clinical-services/siarc/docs/indexc052.html"}}}
*** Index of /sphsc/clinicalservices/siarc/docs {"url":"node/891","options":{"attributes":{"title":"clinical-services/siarc/docs/indexf8a0.html"}}}
* Year Round Evaluations and Treatment at UWSHC {"url":"node/901","options":{"attributes":{"title":"clinical-services/speech-lang-services.html"}}}
** Summer Treatment Program Adult Speech Language Evaluations and Therapy {"url":"node/903","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/adult-speech-language-summer-treatment-program.html"}}}
*** Summer Treatment Program Overview {"url":"node/905","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/ark/assistive-living-devices.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/ark {"url":"node/904","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/ark/index.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/ark {"url":"node/906","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/ark/index30b5.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/ark {"url":"node/907","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/ark/index5851.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/ark {"url":"node/908","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/ark/index72c9.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/ark {"url":"node/909","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/ark/indexad3f.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/ark {"url":"node/910","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/ark/indexb70a.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/ark {"url":"node/911","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/ark/indexbfec.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/ark {"url":"node/912","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/ark/indexc052.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/ark {"url":"node/913","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/ark/indexf8a0.html"}}}
*** Untitled Document {"url":"node/914","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/ark/untitled.html"}}}
** Summer Treatment Program Overview {"url":"node/915","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/assistive-living-devices.html"}}}
** Summer Treatment Program Child Speech Language Evaluations and Therapy {"url":"node/916","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/child-speech-language-summer-treatment-program.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/docs {"url":"node/917","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/docs/index.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/docs {"url":"node/918","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/docs/index30b5.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/docs {"url":"node/919","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/docs/index5851.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/docs {"url":"node/920","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/docs/index72c9.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/docs {"url":"node/921","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/docs/indexad3f.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/docs {"url":"node/922","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/docs/indexb70a.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/docs {"url":"node/923","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/docs/indexbfec.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/docs {"url":"node/924","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/docs/indexc052.html"}}}
*** Index of /sphsc/clinicalservices/summer-treatment-2012/docs {"url":"node/925","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/docs/indexf8a0.html"}}}
** Index of /sphsc/clinicalservices/summer-treatment-2012 {"url":"node/902","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/index.html"}}}
** Index of /sphsc/clinicalservices/summer-treatment-2012 {"url":"node/926","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/index30b5.html"}}}
** Index of /sphsc/clinicalservices/summer-treatment-2012 {"url":"node/927","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/index5851.html"}}}
** Index of /sphsc/clinicalservices/summer-treatment-2012 {"url":"node/928","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/index72c9.html"}}}
** Index of /sphsc/clinicalservices/summer-treatment-2012 {"url":"node/929","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/indexad3f.html"}}}
** Index of /sphsc/clinicalservices/summer-treatment-2012 {"url":"node/930","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/indexb70a.html"}}}
** Index of /sphsc/clinicalservices/summer-treatment-2012 {"url":"node/931","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/indexbfec.html"}}}
** Index of /sphsc/clinicalservices/summer-treatment-2012 {"url":"node/932","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/indexc052.html"}}}
** Index of /sphsc/clinicalservices/summer-treatment-2012 {"url":"node/933","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/indexf8a0.html"}}}
** Summer Treatment Program Overview {"url":"node/934","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/summer-treatment-program-overview.html"}}}
** Summer Treatment Program Registration Procedure {"url":"node/935","options":{"attributes":{"title":"clinical-services/summer-treatment-2012/summer-treatment-registration-information.html"}}}
* Summer 2015 Intensive Stuttering Workshop for Teens {"url":"node/936","options":{"attributes":{"title":"clinical-services/summer-treatment-children.html"}}}
* Taking the Bus {"url":"node/937","options":{"attributes":{"title":"clinical-services/taking-the-bus.html"}}}
Community Partners {"url":"node/272","options":{"attributes":{"title":"community-partners"}}}
* Community Partners {"url":"node/938","options":{"attributes":{"title":"community-partners/index-2.html"}}}
* Not Found - compart/index.html {"url":"node/939","options":{"attributes":{"title":"compart/index.html"}}}
How to Contact Us {"url":"node/940","options":{"attributes":{"title":"contact.html"}}}
Current Students
* Quick Links - current-students/aud-degree-planning.html {"url":"node/943","options":{"attributes":{"title":"current-students/aud-degree-planning.html"}}}
* Quick Links - current-students/aud-licensure.html {"url":"node/944","options":{"attributes":{"title":"current-students/aud-licensure.html"}}}
* Quick Links - current-students/aud-praxis.html {"url":"node/945","options":{"attributes":{"title":"current-students/aud-praxis.html"}}}
* Quick Links - current-students/aud-steps-to-graduate.html {"url":"node/946","options":{"attributes":{"title":"current-students/aud-steps-to-graduate.html"}}}
* Quick Links - current-students/aud.html {"url":"node/942","options":{"attributes":{"title":"current-students/aud.html"}}}
* Quick Links - current-students/capstone.html {"url":"node/947","options":{"attributes":{"title":"current-students/capstone.html"}}}
* Quick Links - current-students/concerns-grievances.html {"url":"node/948","options":{"attributes":{"title":"current-students/concerns-grievances.html"}}}
* Quick Links - current-students/core-steps-to-graduate.html {"url":"node/949","options":{"attributes":{"title":"current-students/core-steps-to-graduate.html"}}}
* Quick Links - current-students/core-thesis-option.html {"url":"node/950","options":{"attributes":{"title":"current-students/core-thesis-option.html"}}}
* Quick Links - current-students/coreslp-degree-planning.html {"url":"node/952","options":{"attributes":{"title":"current-students/coreslp-degree-planning.html"}}}
* Quick Links - current-students/coreslp.html {"url":"node/951","options":{"attributes":{"title":"current-students/coreslp.html"}}}
* Quick Links - current-students/counseling-wellness.html {"url":"node/953","options":{"attributes":{"title":"current-students/counseling-wellness.html"}}}
* Academic and Student Services Eagleson Hall {"url":"node/954","options":{"attributes":{"title":"current-students/current-academic-student-services.html"}}}
* Current Students UW NSSLHA Newsletters {"url":"node/956","options":{"attributes":{"title":"current-students/current-nsslha-newsletters.html"}}}
* Current Students National Student Speech, Language and Hearing Association (NSSLHA) {"url":"node/955","options":{"attributes":{"title":"current-students/current-nsslha.html"}}}
* Current Students Student Academy of Audiology (SAA) {"url":"node/957","options":{"attributes":{"title":"current-students/current-student-academy-of-audiology.html"}}}
* Quick Links - current-students/disability-resources.html {"url":"node/958","options":{"attributes":{"title":"current-students/disability-resources.html"}}}
* PLACEHOLDER - docs {"url":"node/960","options":{"attributes":{"title":"current-students/docs"}}}
** Not Found - current-students/docs/AuD-Registration-Tuition-Policy-Rev2014.html {"url":"node/959","options":{"attributes":{"title":"current-students/docs/AuD-Registration-Tuition-Policy-Rev2014.html"}}}
** Not Found - current-students/docs/NSSLHA-April-2013.html {"url":"node/961","options":{"attributes":{"title":"current-students/docs/NSSLHA-April-2013.html"}}}
* Not Found - current-students/grad.washington.html {"url":"node/962","options":{"attributes":{"title":"current-students/grad.washington.html"}}}
* Quick Links - current-students/hybrid-registration.html {"url":"node/963","options":{"attributes":{"title":"current-students/hybrid-registration.html"}}}
* Quick Links - current-students/index-2.html {"url":"node/964","options":{"attributes":{"title":"current-students/index-2.html"}}}
* Quick Links - current-students/index.html {"url":"node/941","options":{"attributes":{"title":"current-students/index.html"}}}
* Not Found - current-students/loader.html {"url":"node/965","options":{"attributes":{"title":"current-students/loader.html"}}}
* Quick Links - current-students/med-steps-to-graduate.html {"url":"node/966","options":{"attributes":{"title":"current-students/med-steps-to-graduate.html"}}}
* Quick Links - current-students/med-thesis-option.html {"url":"node/967","options":{"attributes":{"title":"current-students/med-thesis-option.html"}}}
* Quick Links - current-students/medslp-degree-planning.html {"url":"node/969","options":{"attributes":{"title":"current-students/medslp-degree-planning.html"}}}
* Quick Links - current-students/medslp-licensure.html {"url":"node/970","options":{"attributes":{"title":"current-students/medslp-licensure.html"}}}
* Quick Links - current-students/medslp-praxis.html {"url":"node/971","options":{"attributes":{"title":"current-students/medslp-praxis.html"}}}
* Quick Links - current-students/medslp.html {"url":"node/968","options":{"attributes":{"title":"current-students/medslp.html"}}}
** PLACEHOLDER - docs {"url":"node/974","options":{"attributes":{"title":"current-students/new-graduate-students/docs"}}}
*** Not Found - current-students/new-graduate-students/docs/Graduate-Student-Guide-Rev2014.html {"url":"node/973","options":{"attributes":{"title":"current-students/new-graduate-students/docs/Graduate-Student-Guide-Rev2014.html"}}}
** Quick Links - current-students/new-graduate-students/index.html {"url":"node/972","options":{"attributes":{"title":"current-students/new-graduate-students/index.html"}}}
** PLACEHOLDER - ms aud guide {"url":"node/976","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/academic-requirements.html {"url":"node/975","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/academic-requirements.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/accommodations.html {"url":"node/977","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/accommodations.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/background-check.html {"url":"node/978","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/background-check.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/bbp.html {"url":"node/979","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/bbp.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/clinical-compliance.html {"url":"node/980","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/clinical-compliance.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/compliance-forms.html {"url":"node/981","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/compliance-forms.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/compliance-policy.html {"url":"node/982","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/compliance-policy.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/cpr.html {"url":"node/983","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/cpr.html"}}}
*** PLACEHOLDER - docs {"url":"node/985","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/docs"}}}
**** Not Found - current-students/new-graduate-students/ms-aud-guide/docs/Graduate-Student-Guide-Rev2014.html {"url":"node/984","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/docs/Graduate-Student-Guide-Rev2014.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/enrollment-confirmation.html {"url":"node/986","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/enrollment-confirmation.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/final-transcripts.html {"url":"node/987","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/final-transcripts.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/financial-aid.html {"url":"node/988","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/financial-aid.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/hazardous-waste.html {"url":"node/989","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/hazardous-waste.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/hipaa.html {"url":"node/990","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/hipaa.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/housing.html {"url":"node/991","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/housing.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/hsip.html {"url":"node/992","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/hsip.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/important-dates.html {"url":"node/993","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/important-dates.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/insurance.html {"url":"node/994","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/insurance.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/netid.html {"url":"node/995","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/netid.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/observation-hours.html {"url":"node/996","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/observation-hours.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/orientation.html {"url":"node/997","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/orientation.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/other-info.html {"url":"node/998","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/other-info.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/prerequisite-coursework.html {"url":"node/999","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/prerequisite-coursework.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/readings.html {"url":"node/1000","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/readings.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/registration.html {"url":"node/1001","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/registration.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/student-resources.html {"url":"node/1002","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/student-resources.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/technology.html {"url":"node/1003","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/technology.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/transportation.html {"url":"node/1004","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/transportation.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/tuition-fees.html {"url":"node/1005","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/tuition-fees.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/typhon.html {"url":"node/1006","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/typhon.html"}}}
*** Quick Links - current-students/new-graduate-students/ms-aud-guide/welcome.html {"url":"node/1007","options":{"attributes":{"title":"current-students/new-graduate-students/ms-aud-guide/welcome.html"}}}
** PLACEHOLDER - phd guide {"url":"node/1009","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/academic-requirements.html {"url":"node/1008","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/academic-requirements.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/accommodations.html {"url":"node/1010","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/accommodations.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/background-check.html {"url":"node/1011","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/background-check.html"}}}
*** PLACEHOLDER - docs {"url":"node/1013","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/docs"}}}
**** Not Found - current-students/new-graduate-students/phd-guide/docs/Graduate-Student-Guide-Rev2014.html {"url":"node/1012","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/docs/Graduate-Student-Guide-Rev2014.html"}}}
**** Not Found - current-students/new-graduate-students/phd-guide/docs/NewStudentChecklist-PhD-2015.html {"url":"node/1014","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/docs/NewStudentChecklist-PhD-2015.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/enrollment-confirmation.html {"url":"node/1015","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/enrollment-confirmation.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/final-transcripts.html {"url":"node/1016","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/final-transcripts.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/forms.html {"url":"node/1017","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/forms.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/hipaa.html {"url":"node/1018","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/hipaa.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/housing.html {"url":"node/1019","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/housing.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/immunizations.html {"url":"node/1020","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/immunizations.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/important-dates.html {"url":"node/1021","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/important-dates.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/netid.html {"url":"node/1022","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/netid.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/orientation.html {"url":"node/1023","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/orientation.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/other-info.html {"url":"node/1024","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/other-info.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/registration.html {"url":"node/1025","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/registration.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/student-resources.html {"url":"node/1026","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/student-resources.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/ta-ra-conference.html {"url":"node/1027","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/ta-ra-conference.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/technology.html {"url":"node/1028","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/technology.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/tuition-fees.html {"url":"node/1029","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/tuition-fees.html"}}}
*** Quick Links - current-students/new-graduate-students/phd-guide/welcome.html {"url":"node/1030","options":{"attributes":{"title":"current-students/new-graduate-students/phd-guide/welcome.html"}}}
* Quick Links - current-students/pce-registration.html {"url":"node/1031","options":{"attributes":{"title":"current-students/pce-registration.html"}}}
* Academic and Student Services Eagleson Hall {"url":"node/1032","options":{"attributes":{"title":"current-students/racademic-and-student-services.html"}}}
* Quick Links - current-students/slp-licensure.html {"url":"node/1033","options":{"attributes":{"title":"current-students/slp-licensure.html"}}}
* Quick Links - current-students/slp-praxis.html {"url":"node/1034","options":{"attributes":{"title":"current-students/slp-praxis.html"}}}
* Quick Links - current-students/state-registration.html {"url":"node/1035","options":{"attributes":{"title":"current-students/state-registration.html"}}}
* Quick Links - current-students/thesis-submission.html {"url":"node/1036","options":{"attributes":{"title":"current-students/thesis-submission.html"}}}
Directory 
* Faculty Profiles {"url":"node/1038","options":{"attributes":{"title":"directory/bierer.html"}}}
* Faculty Profiles {"url":"node/1039","options":{"attributes":{"title":"directory/dowden.html"}}}
* Faculty Profiles {"url":"node/1040","options":{"attributes":{"title":"directory/eadie.html"}}}
* Faculty Profiles {"url":"node/1041","options":{"attributes":{"title":"directory/folsom.html"}}}
* SPHSC Faculty and Staff Directory {"url":"node/1042","options":{"attributes":{"title":"directory/index-2.html"}}}
* SPHSC Faculty and Staff Directory {"url":"node/1037","options":{"attributes":{"title":"directory/index.html"}}}
* Faculty Profiles {"url":"node/1043","options":{"attributes":{"title":"directory/kover.html"}}}
* Faculty Profiles {"url":"node/1044","options":{"attributes":{"title":"directory/kuhl.html"}}}
* PLACEHOLDER - labsites {"url":"node/1046","options":{"attributes":{"title":"directory/labsites"}}}
** PLACEHOLDER - olswang {"url":"node/1047","options":{"attributes":{"title":"directory/labsites/olswang"}}}
*** Not Found - directory/labsites/olswang/research.html {"url":"node/1045","options":{"attributes":{"title":"directory/labsites/olswang/research.html"}}}
* Faculty Profiles {"url":"node/1048","options":{"attributes":{"title":"directory/olswang.html"}}}
* SPHSC Organizational Charts {"url":"node/1049","options":{"attributes":{"title":"directory/organization.html"}}}
* PLACEHOLDER - research {"url":"node/1051","options":{"attributes":{"title":"directory/research"}}}
** Not Found - directory/research/lawresearch.html {"url":"node/1050","options":{"attributes":{"title":"directory/research/lawresearch.html"}}}
* Faculty Profiles {"url":"node/1052","options":{"attributes":{"title":"directory/spencer.html"}}}
* Faculty Profiles {"url":"node/1053","options":{"attributes":{"title":"directory/tremblay.html"}}}
* Faculty Profiles {"url":"node/1054","options":{"attributes":{"title":"directory/werner.html"}}}
Employment 
* Employment {"url":"node/1056","options":{"attributes":{"title":"employment/index-2.html"}}}
* Employment {"url":"node/1055","options":{"attributes":{"title":"employment/index.html"}}}
PLACEHOLDER - incs {"url":"node/1058","options":{"attributes":{"title":"incs"}}}
FeedEk {"url":"node/1057","options":{"attributes":{"title":"incs/FeedEk.js"}}}
About the Department of Speech and Hearing Sciences {"url":"node/1059","options":{"attributes":{"title":"index-2.html"}}}
Not Found - index-3.html {"url":"node/1060","options":{"attributes":{"title":"index-3.html"}}}
About the Department of Speech and Hearing Sciences {"url":"node/718","options":{"attributes":{"title":"index.html"}}}
PLACEHOLDER - labsites {"url":"node/1062","options":{"attributes":{"title":"labsites"}}}
PLACEHOLDER - kendall {"url":"node/1063","options":{"attributes":{"title":"labsites/kendall"}}}
* SPHSC Research {"url":"node/1061","options":{"attributes":{"title":"labsites/kendall/about.html"}}}
* Speech&Hearing {"url":"node/1064","options":{"attributes":{"title":"labsites/kendall/contactus.html"}}}
* Not Found - labsites/kendall/hot link www.ibic.washington.html {"url":"node/1065","options":{"attributes":{"title":"labsites/kendall/hot link www.ibic.washington.html"}}}
* Speech&Hearing {"url":"node/1066","options":{"attributes":{"title":"labsites/kendall/patients.html"}}}
* SPHSC Research {"url":"node/1067","options":{"attributes":{"title":"labsites/kendall/people.html"}}}
* SPHSC Research {"url":"node/1068","options":{"attributes":{"title":"labsites/kendall/photos.html"}}}
* SPHSC Research {"url":"node/1069","options":{"attributes":{"title":"labsites/kendall/publications.html"}}}
* SPHSC Research {"url":"node/1070","options":{"attributes":{"title":"labsites/kendall/research.html"}}}
* SPHSC Research {"url":"node/1071","options":{"attributes":{"title":"labsites/kendall/students.html"}}}
PLACEHOLDER - kendall2 {"url":"node/1073","options":{"attributes":{"title":"labsites/kendall2"}}}
* Not Found - labsites/kendall2/patients.html {"url":"node/1072","options":{"attributes":{"title":"labsites/kendall2/patients.html"}}}
PLACEHOLDER - kover {"url":"node/1075","options":{"attributes":{"title":"labsites/kover"}}}
* Neurodevelopmental Language and Learning Lab {"url":"node/1074","options":{"attributes":{"title":"labsites/kover/about.html"}}}
* Neurodevelopmental Language and Learning Lab {"url":"node/1076","options":{"attributes":{"title":"labsites/kover/contactus.html"}}}
* Neurodevelopmental Language and Learning Lab {"url":"node/1077","options":{"attributes":{"title":"labsites/kover/directions.html"}}}
* Neurodevelopmental Language and Learning Lab {"url":"node/1078","options":{"attributes":{"title":"labsites/kover/families.html"}}}
* Neurodevelopmental Language and Learning Lab {"url":"node/1079","options":{"attributes":{"title":"labsites/kover/lab.html"}}}
* Neurodevelopmental Language and Learning Lab {"url":"node/1080","options":{"attributes":{"title":"labsites/kover/research.html"}}}
* Neurodevelopmental Language and Learning Lab {"url":"node/1081","options":{"attributes":{"title":"labsites/kover/students.html"}}}
PLACEHOLDER - kspencer {"url":"node/1083","options":{"attributes":{"title":"labsites/kspencer"}}}
* Speech&Hearing {"url":"node/1082","options":{"attributes":{"title":"labsites/kspencer/contactus.html"}}}
* SPHSC Research {"url":"node/1084","options":{"attributes":{"title":"labsites/kspencer/people.html"}}}
* SPHSC Research {"url":"node/1085","options":{"attributes":{"title":"labsites/kspencer/presentations.html"}}}
* SPHSC Research {"url":"node/1086","options":{"attributes":{"title":"labsites/kspencer/research.html"}}}
PLACEHOLDER - olswang {"url":"node/1088","options":{"attributes":{"title":"labsites/olswang"}}}
* Speech&Hearing {"url":"node/1087","options":{"attributes":{"title":"labsites/olswang/contactus.html"}}}
* SPHSC Research {"url":"node/1089","options":{"attributes":{"title":"labsites/olswang/digital.html"}}}
* SPHSC Research {"url":"node/1090","options":{"attributes":{"title":"labsites/olswang/methodologies.html"}}}
* SPHSC Research {"url":"node/1091","options":{"attributes":{"title":"labsites/olswang/people.html"}}}
* SPHSC Research {"url":"node/1092","options":{"attributes":{"title":"labsites/olswang/presentations.html"}}}
* SPHSC Research {"url":"node/1093","options":{"attributes":{"title":"labsites/olswang/publications.html"}}}
* SPHSC Research {"url":"node/1094","options":{"attributes":{"title":"labsites/olswang/research.html"}}}
* SPHSC Research {"url":"node/1096","options":{"attributes":{"title":"labsites/olswang/teg-people.html"}}}
* SPHSC Research {"url":"node/1095","options":{"attributes":{"title":"labsites/olswang/teg.html"}}}
Not Found - labsites/sphsc.html {"url":"node/1097","options":{"attributes":{"title":"labsites/sphsc.html"}}}
PLACEHOLDER - teadie {"url":"node/1099","options":{"attributes":{"title":"labsites/teadie"}}}
* Speech&Hearing {"url":"node/1098","options":{"attributes":{"title":"labsites/teadie/contactus.html"}}}
* SPHSC Research {"url":"node/1100","options":{"attributes":{"title":"labsites/teadie/people.html"}}}
* SPHSC Research {"url":"node/1101","options":{"attributes":{"title":"labsites/teadie/presentations.html"}}}
* SPHSC Research {"url":"node/1102","options":{"attributes":{"title":"labsites/teadie/publications.html"}}}
* SPHSC Research {"url":"node/1104","options":{"attributes":{"title":"labsites/teadie/research-participation.html"}}}
* SPHSC Research {"url":"node/1103","options":{"attributes":{"title":"labsites/teadie/research.html"}}}
PLACEHOLDER - medslp {"url":"node/1106","options":{"attributes":{"title":"medslp"}}}
* Not Found - medslp/tuition.html {"url":"node/1105","options":{"attributes":{"title":"medslp/tuition.html"}}}
News and Events 
* News and Events - Distinguisehd Alumni Lecture | UW Speech and Hearing Sciences | Seattle, Washington {"url":"node/1108","options":{"attributes":{"title":"newsevents/alumni-lecuture.html"}}}
* Seminars in Hearing and Communication Sciences {"url":"node/1109","options":{"attributes":{"title":"newsevents/events-seminars.html"}}}
* News and Events -- Featured Story | UW Speech and Hearing Sciences | Seattle, Washington {"url":"node/1110","options":{"attributes":{"title":"newsevents/index-2.html"}}}
* News and Events -- Featured Story | UW Speech and Hearing Sciences | Seattle, Washington {"url":"node/1107","options":{"attributes":{"title":"newsevents/index.html"}}}
* News and Events -- All Stories | UW Speech and Hearing Sciences | Seattle, Washington {"url":"node/1111","options":{"attributes":{"title":"newsevents/news-all.html"}}}
* News and Events -- Archived Stories | UW Speech and Hearing Sciences | Seattle, Washington {"url":"node/1112","options":{"attributes":{"title":"newsevents/news-archived.html"}}}
Outreach 
* Faculty and Staff Awards and Achievements {"url":"node/1114","options":{"attributes":{"title":"outreachdev/awardfac.html"}}}
* Student Awards {"url":"node/1115","options":{"attributes":{"title":"outreachdev/awardstudent.html"}}}
* How YOU Can Help Us Grow {"url":"node/1116","options":{"attributes":{"title":"outreachdev/grow.html"}}}
* Message from the Chair {"url":"node/1117","options":{"attributes":{"title":"outreachdev/index-2.html"}}}
* Message from the Chair {"url":"node/1113","options":{"attributes":{"title":"outreachdev/index.html"}}}
* Aphasia Day {"url":"node/1118","options":{"attributes":{"title":"outreachdev/outaphasiaday.html"}}}
* Aphasia Newsletter {"url":"node/1119","options":{"attributes":{"title":"outreachdev/outasphasianews.html"}}}
* Dubs and the UW Mail Truck {"url":"node/1120","options":{"attributes":{"title":"outreachdev/outdubs.html"}}}
* The King's Speech {"url":"node/1121","options":{"attributes":{"title":"outreachdev/outkings.html"}}}
* Lion's Club Events {"url":"node/1122","options":{"attributes":{"title":"outreachdev/outlions.html"}}}
* Paws on Science {"url":"node/1123","options":{"attributes":{"title":"outreachdev/outpaws.html"}}}
* UW Employee Hearing Screenings {"url":"node/1124","options":{"attributes":{"title":"outreachdev/outreach-employee-hearing-screening.html"}}}
* Speech and Hearing Sciences’ Outreach in the Community {"url":"node/1125","options":{"attributes":{"title":"outreachdev/outreach-in-the-community.html"}}}
* Special Olympics {"url":"node/1126","options":{"attributes":{"title":"outreachdev/outspecial.html"}}}
* Woodstick Big Beat {"url":"node/1127","options":{"attributes":{"title":"outreachdev/outwoodstick.html"}}}
* Scholarship Funds {"url":"node/1128","options":{"attributes":{"title":"outreachdev/scholarship-funds.html"}}}
* Scholarships and Special Funds {"url":"node/1129","options":{"attributes":{"title":"outreachdev/scholarships-carrell-miner-siva.html"}}}
* Special Funds {"url":"node/1130","options":{"attributes":{"title":"outreachdev/scholarships-friends-of-speech-hearing.html"}}}
* Scholarships and Special Funds {"url":"node/1131","options":{"attributes":{"title":"outreachdev/scholarships-minifie.html"}}}
* Special Funds {"url":"node/1132","options":{"attributes":{"title":"outreachdev/scholarships-neurological-communications-disorders.html"}}}
* Scholarships and Special Funds {"url":"node/1133","options":{"attributes":{"title":"outreachdev/scholarships-olswang.html"}}}
* Scholarships and Special Funds {"url":"node/1134","options":{"attributes":{"title":"outreachdev/scholarships-palmer.html"}}}
* Scholarships and Special Funds {"url":"node/1135","options":{"attributes":{"title":"outreachdev/scholarships-public-schools-slp-endowed.html"}}}
* Scholarships and Special Funds {"url":"node/1136","options":{"attributes":{"title":"outreachdev/scholarships-siva-endowed.html"}}}
* Scholarships and Special Funds {"url":"node/1137","options":{"attributes":{"title":"outreachdev/scholarships-slp-neurological.html"}}}
* Special Funds {"url":"node/1138","options":{"attributes":{"title":"outreachdev/scholarships-speech-hearing-clinic-special-needs.html"}}}
* Scholarships and Special Funds {"url":"node/1139","options":{"attributes":{"title":"outreachdev/scholarships-yantis.html"}}}
* Special Funds {"url":"node/1140","options":{"attributes":{"title":"outreachdev/special-funds.html"}}}
* THE DEPARTMENT OF SPEECH AND HEARING SCIENCES VISION, PROGRAM GOALS AND STRATEGIC PLAN {"url":"node/1141","options":{"attributes":{"title":"outreachdev/strategic-plan.html"}}}
Research
* Affiliated Centers and Laboratories {"url":"node/1143","options":{"attributes":{"title":"research/aff-labs.html"}}}
* Speech and Hearing Sciences Research {"url":"node/1144","options":{"attributes":{"title":"research/index-2.html"}}}
* Speech and Hearing Sciences Research {"url":"node/1142","options":{"attributes":{"title":"research/index.html"}}}
* Research Areas {"url":"node/1145","options":{"attributes":{"title":"research/res-areas.html"}}}
* Research Labs {"url":"node/1146","options":{"attributes":{"title":"research/res-labs.html"}}}
PLACEHOLDER - sitewide {"url":"node/1148","options":{"attributes":{"title":"sitewide"}}}
Speech and Hearing Science Intranet {"url":"node/1147","options":{"attributes":{"title":"sitewide/intranet.html"}}}








****/
?>


