#!/usr/bin/env drush
<?php 

include('sphsc-helpers.php'); 


node_delete(272);

sphsc1();
return; 





function sphsc1() {


	$query = db_select('node', 'n') 
		->fields('n')
		->condition('n.type', 'page')
		->orderBy('n.nid', 'desc'); 
	$qresult = $query->execute()->fetchAll();

	foreach($qresult as $k => $data) {

		print "\n#####". $data->nid ."|". $data->type."|". $data->title; 

		$node = node_load($data->nid); 
		print (empty($node->nid) ? "\n\tMISS:". $data->nid: "");
		print "\n\t". $node->nid ."|". $node->type."|". $node->title; 

	/* 
		$node->revision = 0; 

		$side = $node->field_sidebar_body['und'][0]['value']; 
		$sside = str_replace(array("\n", "\r"), '', strip_tags($side));
		$sbody = str_replace(array("\n", "\r"), '', strip_tags($node->body['und'][0]['value']));
		if(stripos($side, '<iframe') !== false || stripos($side, '<form') !== false) {
			$node->revision = 1; 
			$node->field_sidebar_body['und'][0]['value'] = '';		
		}

		if(empty($sside) && empty($sbody)) {
			$node->revision = 1; 
			$node->published = 0; 
			$node->comment = "Empty content? Unpublishing....";
		}

		if($node->revision === 1) {
			
			print_r(array($node->nid, $node->title, substr($sside, 0, 200), substr($sbody, 0, 200),)); 
			node_save($node); 

		}
	*/
	}


}




function sphsc2() {

	$query = db_select('node', 'n');
	$query->fields('n')
		->condition('n.type', 'sidebar_snippet')
		->orderBy('n.nid', 'desc'); 
	$results = $query->execute()->fetchAll();

	foreach($results as $k => $data) {

		$node = node_load($data->nid); 

		$n = preg_match_all('/src="([^"]+\.jpe?g|png|gif)"/', $node->body['und'][0]['value'], $a);
		$matches = empty($a[1]) ? array() : $a[1]; 

		foreach($matches as $k => $v) {

			$dir = '/sites/default/files/sphsc_module/'; 
			$new_url = $dir . basename($v); 

			//$save_result = save_image_to_local('https://content.sphsc.washington.edu/sphintra/web2/'. $v, $dir); 
			$node->body['und'][0]['value'] = str_replace($v, $new_url, $node->body['und'][0]['value']);

			node_save($node);

		}

	//	print_r(array($node->body['und'][0]['value'], $matches));

	}
}


function sphsc3() {


	$results = array(); 
	$i = 0;
	do {

		// 'https://content.sphsc.washington.edu/sphintra/web2/factoidsaud.asp'; //7-8
		// 'https://content.sphsc.washington.edu/sphintra/web2/res_blurb.asp';  // 11 
		// 'https://content.sphsc.washington.edu/sphintra/web2/factoidscore.asp'; // 14 
		// 'https://content.sphsc.washington.edu/sphintra/web2/factoidsphd.asp'; //15 
		// 'https://content.sphsc.washington.edu/sphintra/web2/factoidspb.asp';  // 16, 19 
		// 'https://content.sphsc.washington.edu/sphintra/web2/factoidsmed.asp';  // 17, 20
		// 'https://content.sphsc.washington.edu/sphintra/web2/factoidsug.asp'; // 18 
		// 'https://content.sphsc.washington.edu/sphintra/web2/res_stone_blurb.asp';
		//$uri = 'https://content.sphsc.washington.edu/sphintra/web2/factoidscore.asp'; // 19 
		// No's: 
		// http://content.sphsc.washington.edu/sphintra/web2/clinic_msg.asp
		// http://content.sphsc.washington.edu/sphintra/web2/home_right.asp
		// https://content.sphsc.washington.edu/sphintra/web2/res_stone_blurb.asp
		// http://content.sphsc.washington.edu/sphintra/web2/outreach_msg.asp

		$tids = array();


		$c = array(
			'indent' => true,
			'output-xhtml' => true,
			'wrap' => 200,
			'hide-comments' => true, 
		);


		$response = get_page($uri);
		$tidy = new tidy;
		$tidy->parseString($response, $c, 'utf8');
		$tidy->cleanRepair();
		preg_match("/<body[^>]*>(.*?)<\/body>/is", $tidy, $a);

		$body = str_replace(array("\n", "\r"), '', $a[1]);
		$title = some_words(strip_tags($body)); 

		$results[ $title ] = $body; 


	} while ($i++ < 20);

	foreach($results as $k => $v) {
		save_item($k, $v, $tids);
	}

}



function save_item($title, $body_text = false, $sidebar_text = false, $tids = array()) {

	return; 

	$node = new stdClass();
	$node->type = 'sidebar_snippet';
	node_object_prepare($node);

	$node->title = $title;
	$node->language = LANGUAGE_NONE;
	$node->uid = 1;
	$node->status = 1; 

	if($body_text !== false) {
		$node->body[$node->language][0]['value'] = $body_text;
		$node->body[$node->language][0]['format'] = 'full_html';
	}
	if($sidebar_text !== false) {

		$node->body[$node->language][0]['field_sidebar_body']['value'] = $sidebar_snippet;  
		$node->body[$node->language][0]['field_sidebar_body']['format'] = 'full_html';
	}
	foreach($tids as $k => $v) {
			$node->field_tags[$node->language][]['tid'] = $v; 
	}

	// $path = 'content/programmatically_created_node_' . date('YmdHis');
	// $node->path = array('alias' => $path);

	node_save($node);
}



function sphsc4() {

	/*******
	$results = db_select('node', 'n')
		->fields('n')
		//->orderBy('length(alias)', 'ASC')
		->condition('n.title', '%placeholder%', 'like')
		->execute()
		->fetchAll();
		
	foreach($results as $k => $data) {
		print "\n". $data->nid;
		var_dump(node_delete($data->nid)); 
	}

	exit;*/


	$query = db_select('url_alias', 'a');		
	$query->join('menu_links', 'm', 'm.link_path = a.source');

	$query->fields('a')
		->fields('m')
		//->condition('a.alias', $path)
		->condition('link_title', 'Scholarships & Special Funds')
		->orderBy('length(a.alias)', 'ASC');





	$results = $query->execute()->fetchAll();

	//print_r($results);


	// db_insert('custom_table_name')->fields(array(
	// 	'id' => $account->uid,
	// 	'text' => 'some text'
	// ))->execute();


	foreach($results as $k => $data) {

		continue; 
		
		$path = explode('/', $data->alias);
		$n = count($path); 
		$suffix = $n > 1 ? $path[ $n - 1 ] : $data->alias;
		$suffix = $n > 2 ? $path[ $n - 2 ] ." - ". $path[ $n - 1 ] : $data->alias;

		$data->link_title = $data->link_title ." -- ". title_case($suffix); 

		print "\n". $data->link_title;

		$updates[] = db_update('menu_links')->fields(array(
			'link_title' => $data->link_title
		))->condition('mlid', $data->mlid)->execute();

		$updates[] = db_update('node')->fields(array(
			'title' => $data->link_title
		))->condition('nid', str_replace('node/', '', $data->source))->execute();

	}

}