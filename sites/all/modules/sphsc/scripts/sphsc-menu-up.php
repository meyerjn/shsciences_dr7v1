#!/usr/bin/env drush
<?php 



$aliases = db_select('url_alias', 'a')
	->fields('a')
	->orderBy('length(alias)', 'ASC')
	->execute()
	->fetchAll();


// print_r($aliases);


$i = $m = 0; 

do {

	foreach($aliases as $k => $alias) {

		continue; 

		$j = str_replace('node/', '', $alias->source); 
		$node = node_load($j);
		$path = dirname($alias->alias); 
		$level = substr_count($path, '/'); 

		print "\n\n$level|$path"; 

		if(empty($node->nid) || $node->nid < 250) {
			continue;
		}

		if($level === 0) {

			$m++; 
			$node->menu = array(
				'link_title' => $node->title,
				'menu_name' => 'main-menu',
				'plid' => 0,							// <------ Root item 
				'enabled' => 1,
			);

			print "\n\t". $node-nid ."|". $node->title ."|". json_encode($node->menu); 
			node_save($node);

		}
		else  { 

			$ml = db_select('url_alias', 'a');		
			$ml->join('menu_links', 'm', 'm.link_path = a.source');
			$ml->fields('a')
				->fields('m')
				->condition('a.alias', $path)
				->orderBy('length(a.alias)', 'ASC');

			$parents = $ml->execute()->fetchAll();

			if(!empty($parents[0]->mlid)) {

				$m++; 
				print "\n\Parents: ". join("|", $parents);  

				$node->menu = array(
					'link_title' => $node->title,
					'menu_name' => 'main-menu',
					'plid' => $parents[0]->mlid,			// <------ Parent item 
					'enabled' => 1,
				);

				print "\n\t". $node-nid ."|". $node->title ."|". json_encode($node->menu); 
				node_save($node);

			}
		}
	}

	print "\nTry ". $i ." ". count($results) .' results, '. $m .' matches'; 

} while($i++ < 6);


//	$node = node_load($data['nid']); 



