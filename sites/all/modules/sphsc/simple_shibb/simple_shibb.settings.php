<?php



$settings = array(

	'hosts' => array( 

		'*' => array(
		
			'sp_login_url' => '/Shibboleth.sso/Login', //DS
			'sp_logout_url' => '/Shibboleth.sso/Logout',
			'sp_email_variable' => 'uwNetID',
			'sp_username_variable' => 'uwEduEmail',
			'sp_roles_variable' => false,
			'force_https_login' => false, 
			'login_text' => 'Login with your UW NetID',
			'link_accounts' => true, 
			//'debug_path' => false, 
			//'enable_passive' => false, 
			'logout_expired_sessions' => false, 
		), 

	),

);




