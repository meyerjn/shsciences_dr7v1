<?php
/**
 * @file
 * sphsc_webforms.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function sphsc_webforms_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_webform';
  $strongarm->value = '0';
  $export['language_content_type_webform'] = $strongarm;

  return $export;
}
