<?php
/**
 * @file
 * sphsc_structure.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function sphsc_structure_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['aggregator-feed-1'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'feed-1',
    'module' => 'aggregator',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => 0,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['backup_migrate-quick_backup'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'quick_backup',
    'module' => 'backup_migrate',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -26,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['calendar-calendar_legend'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'calendar_legend',
    'module' => 'calendar',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -26,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['context_ui-devel'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'devel',
    'module' => 'context_ui',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -24,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['context_ui-editor'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'editor',
    'module' => 'context_ui',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -25,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['devel-execute_php'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'execute_php',
    'module' => 'devel',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -22,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['devel-switch_user'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'switch_user',
    'module' => 'devel',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -8,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['footer_sitemap-footer_sitemap'] = array(
    'cache' => 4,
    'custom' => 0,
    'delta' => 'footer_sitemap',
    'module' => 'footer_sitemap',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'sphsc',
        'weight' => -26,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['locale-language'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'language',
    'module' => 'locale',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -18,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-devel'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'devel',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -23,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-features'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'features',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -21,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-menu-go-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-go-menu',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => 0,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-menu-header-action-items'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-header-action-items',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -20,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu_block-1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 1,
    'module' => 'menu_block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => 'sidebar_left',
        'status' => 1,
        'theme' => 'sphsc',
        'weight' => -26,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['node-recent'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'recent',
    'module' => 'node',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'dashboard_main',
        'status' => 1,
        'theme' => 'seven',
        'weight' => -24,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -12,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['node-syndicate'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'syndicate',
    'module' => 'node',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -7,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['og_extras-group_info'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'group_info',
    'module' => 'og_extras',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'sphsc',
        'weight' => -22,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['og_extras-node_links'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'node_links',
    'module' => 'og_extras',
    'node_types' => array(
      0 => 'sphsc_group_page',
      1 => 'sphsc_website_group',
    ),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'sphsc',
        'weight' => -25,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['search-form'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'form',
    'module' => 'search',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => -1,
      ),
      'seven' => array(
        'region' => 'dashboard_sidebar',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'sphsc',
        'weight' => -26,
      ),
      'uw_boundless' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'uw_boundless',
        'weight' => -1,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['shortcut-shortcuts'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'shortcuts',
    'module' => 'shortcut',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -9,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-help'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'help',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'help',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'help',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => 'help',
        'status' => 1,
        'theme' => 'sphsc',
        'weight' => 0,
      ),
      'uw_boundless' => array(
        'region' => 'help',
        'status' => 1,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'sphsc',
        'weight' => -19,
      ),
      'uw_boundless' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-main-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'main-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -17,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-management'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'management',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -16,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-navigation'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'navigation',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -15,
      ),
      'uw_boundless' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-powered-by'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'powered-by',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 10,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 10,
      ),
      'sphsc' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'sphsc',
        'weight' => -23,
      ),
      'uw_boundless' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'uw_boundless',
        'weight' => 10,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['system-user-menu'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'user-menu',
    'module' => 'system',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'dashboard_sidebar',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -6,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['user-login'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'login',
    'module' => 'user',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 10,
      ),
      'sphsc' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'sphsc',
        'weight' => -23,
      ),
      'uw_boundless' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['user-new'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'new',
    'module' => 'user',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'dashboard_sidebar',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 1,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => 1,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['user-online'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'online',
    'module' => 'user',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'dashboard_sidebar',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 2,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => 2,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views--exp-sphsc_articles-page'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '-exp-sphsc_articles-page',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '*recent-news*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'sphsc',
        'weight' => -24,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-632f0ac1ac184095abc54d4a088b7a8c'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '632f0ac1ac184095abc54d4a088b7a8c',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -3,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-9cf22433e0efbd8b206c8e4c2edb177c'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '9cf22433e0efbd8b206c8e4c2edb177c',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => -23,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => 0,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-og_extras_groups-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'og_extras_groups-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'about/*
*group*',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'sphsc',
        'weight' => -18,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-og_extras_members-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'og_extras_members-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(
      'Faculty-Staff Employee' => 6,
      'Site Editor' => 4,
      'Site Manager' => 8,
      'Student Employee' => 7,
    ),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'sphsc',
        'weight' => -19,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-sphsc_admin_content-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'sphsc_admin_content-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'dashboard_sidebar',
        'status' => 1,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -11,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-sphsc_admin_content-block_2'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'sphsc_admin_content-block_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => 'dashboard_main',
        'status' => 1,
        'theme' => 'seven',
        'weight' => -25,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => 0,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-sphsc_group_pages-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'sphsc_group_pages-block',
    'module' => 'views',
    'node_types' => array(
      0 => 'sphsc_group_page',
      1 => 'sphsc_website_group',
    ),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'sphsc',
        'weight' => -20,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-sphsc_group_pages-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'sphsc_group_pages-block_1',
    'module' => 'views',
    'node_types' => array(
      0 => 'sphsc_group_page',
      1 => 'sphsc_website_group',
    ),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'sphsc',
        'weight' => -21,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-sphsc_sidebar_snippet-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'sphsc_sidebar_snippet-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '<front>',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'sphsc',
        'weight' => -26,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-workbench_current_user-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'workbench_current_user-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -2,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-workbench_edited-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'workbench_edited-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => -1,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['webform-client-block-2244'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'client-block-2244',
    'module' => 'webform',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'sphsc' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'sphsc',
        'weight' => 0,
      ),
      'uw_boundless' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'uw_boundless',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['workbench-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'block',
    'module' => 'workbench',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => -99,
      ),
      'seven' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'seven',
        'weight' => -99,
      ),
      'sphsc' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'sphsc',
        'weight' => -26,
      ),
      'uw_boundless' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'uw_boundless',
        'weight' => -99,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  return $export;
}
