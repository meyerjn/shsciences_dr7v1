<?php
/**
 * @file
 * sphsc_structure.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function sphsc_structure_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'front-page';
  $context->description = 'SPHSC Context settings for the front page. ';
  $context->tag = 'front';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('SPHSC Context settings for the front page. ');
  t('front');
  $export['front-page'] = $context;

  return $export;
}
