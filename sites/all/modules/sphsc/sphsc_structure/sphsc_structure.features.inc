<?php
/**
 * @file
 * sphsc_structure.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sphsc_structure_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function sphsc_structure_node_info() {
  $items = array(
    'article' => array(
      'name' => t('Department News'),
      'base' => 'node_content',
      'description' => t('Use <em>articles</em> for time-sensitive content like news, press releases or blog posts.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'file_asset' => array(
      'name' => t('File Asset'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'hero_image' => array(
      'name' => t('Hero Image'),
      'base' => 'node_content',
      'description' => t('Images for the home page banner and scroller.  Choose "Published" and "Promoted to front" to ensure this image appears.  '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'page' => array(
      'name' => t('Basic page'),
      'base' => 'node_content',
      'description' => t('Use <em>basic pages</em> for your static content, such as an \'About us\' page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'schemaorg_person' => array(
      'name' => t('People and Faces'),
      'base' => 'node_content',
      'description' => t('A page representing a person'),
      'has_title' => '1',
      'title_label' => t('Page Title'),
      'help' => '',
    ),
    'sidebar_snippet' => array(
      'name' => t('Sidebar Snippet'),
      'base' => 'node_content',
      'description' => t('Random image or testimonial for pages'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'sphsc_group_page' => array(
      'name' => t('SPHSC Group Page'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'sphsc_website_group' => array(
      'name' => t('SPHSC Website Group'),
      'base' => 'node_content',
      'description' => t('Content type for Organic Groups. Any new nodes of this type can be assigned members, have members edit subpages and so on.  A page with group settings attached. '),
      'has_title' => '1',
      'title_label' => t('Group Title'),
      'help' => '',
    ),
    'webform' => array(
      'name' => t('Webform'),
      'base' => 'node_content',
      'description' => t('Create a new form or questionnaire accessible to users. Submission results and statistics are recorded and accessible to privileged users.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
