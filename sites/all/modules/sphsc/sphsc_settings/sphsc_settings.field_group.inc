<?php
/**
 * @file
 * sphsc_settings.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function sphsc_settings_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_directory_page_fields|node|schemaorg_person|form';
  $field_group->group_name = 'group_directory_page_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'schemaorg_person';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Directory Information',
    'weight' => '1',
    'children' => array(
      0 => 'field_schemaorg_jobtitle',
      1 => 'field_full_name',
      2 => 'field_email_address',
      3 => 'field_telephone',
      4 => 'field_related_drupal_account',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Directory Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_directory_page_fields|node|schemaorg_person|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sphsc_admin_other_fields|node|page|form';
  $field_group->group_name = 'group_sphsc_admin_other_fields';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_sphsc_admin_other';
  $field_group->data = array(
    'label' => 'Other Settings',
    'weight' => '4',
    'children' => array(),
    'format_type' => 'div',
    'format_settings' => array(
      'formatter' => 'open',
      'instance_settings' => array(
        'description' => '',
        'show_label' => 1,
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
        'classes' => '',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $export['group_sphsc_admin_other_fields|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sphsc_admin_other|node|page|form';
  $field_group->group_name = 'group_sphsc_admin_other';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Other Settings',
    'weight' => '3',
    'children' => array(
      0 => 'group_sphsc_admin_other_fields',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Other Settings',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_sphsc_admin_other|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sphsc_field_group|node|page|form';
  $field_group->group_name = 'group_sphsc_field_group';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_sphsc_other';
  $field_group->data = array(
    'label' => 'Other Page Elements',
    'weight' => '4',
    'children' => array(
      0 => 'field_sidebar_body',
      1 => 'field_tags',
      2 => 'field_masthead_image',
      3 => 'field_masthead_title',
      4 => 'path',
      5 => 'redirect',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Other Page Elements',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => '',
        'description' => '',
        'show_label' => '1',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_sphsc_field_group|node|page|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sphsc_other|node|page|form';
  $field_group->group_name = 'group_sphsc_other';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'page';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Other Fields',
    'weight' => '2',
    'children' => array(
      0 => 'group_sphsc_field_group',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Other Fields',
      'instance_settings' => array(
        'required_fields' => 0,
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_sphsc_other|node|page|form'] = $field_group;

  return $export;
}
