/* 

	Customizing the Drupal CKEditor options. 
	@link http://docs.ckeditor.com/#!/guide/dev_configuration
	@link http://ckeditor.com/forums/Support/Remove-style-attribute-paste


*/


CKEDITOR.editorConfig = function(config) {


    // config.language = 'fr';
    // config.uiColor = '#AADC6E';

    config.removePlugins = 'elementspath';

    //config.pasteFilter = 'h1 h2 p ul ol li; img[!src, alt]; a[!href]';

    config.pasteFromWordRemoveStyles = true;
    config.pasteFromWordRemoveFontStyles = true; 

    //config.toolbarStartupExpanded = true;

    // config.allowedContent = {
    //     "div h1 h2 h3 h4 h5 h6 ol p pre ul": {
    //         propertiesOnly: !0,
    //         //styles: !a ? "margin-left,margin-right" : null,
    //         classes: a ? d.config.indentClasses : null
    //     }
    // };

};
