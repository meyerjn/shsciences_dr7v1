<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains: 
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */ 


//sphsc($view);
//sphsc($row); 




?>
<div class="hero-wrapper container">
	<div class="hero-slides">
		<?php 



	foreach($view->result as $node_key => $node) {

		if(!empty($node->field_field_hero_image[0]['raw']['uri'])) {

			foreach($node->field_field_hero_image as $image_key => $image) {
			
			
				$title = empty($node->field_field_hero_link[0]['raw']['url']) ? 
						$node->node_title :
						theme('link', array(
							'#theme' => 'link',
							'#text' => $node->node_title,
							'#path' => $node->field_field_hero_link[0]['raw']['url'],
							'#options' => array(
								'attributes' => array('class' => array(''), 'id' => '', 'title' => $node->node_title,),
								'html' => FALSE,
							),
						));

				$image = theme('image', array( 
					'style_name' => 'front-hero', 
					'path' => $image['raw']['uri'], 
					'alt' => $node->node_title, 
					'title' => $node->node_title, 
					'class' => 'image', 
					));

				print '<div class="hero-slide slide-'. $node_key .' slide-bodies-'. count($node->field_field_hero_body) .'" data-nid="'. $node->nid .'" data-result-n="'. $node_key .'" data-image-n="'. $image_key .'">';
				print '<div class="image">'. $image .'</div>';			
				print '<div class="title"><em>'. $title .'</em><a class="fa fa-camera-retro fa-2x"></a></div>';

				// if(!empty($node->field_field_hero_body[0]['rendered']['#markup'])) {
				// 	foreach($node->field_field_hero_body as $body_key => $body) {

				// 		print '<div class="body body-'. $body_key .'">'. $body['rendered']['#markup'] .'</div>'; 

				// 	}
				// }

				print '<div class="bg-hash"></div>';
				print '</div>';


			}
		}



	}




	?>
	</div>

</div>



