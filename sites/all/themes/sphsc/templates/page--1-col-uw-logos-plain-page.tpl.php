<?php
/**
 * @file
 * SPHSC custom template 
 * 
 * Template name: 1 Column, UW Logos 
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template normally located in the
 * modules/system directory.
 *
 *  
 * 
 */
 ?>


 <div id="uw-container" class="<?php print $page_classes?>">
    <?php /*include_once $directory . "/templates/includes/quicklinks.php";*/ ?>

    <div id="uw-container-inner">
        <?php /*include_once $directory . "/templates/includes/thinstrip.php";*/ ?>

        <?php /*include_once $directory . "/templates/includes/dwgdrops.php";*/ ?>

        <?php include_once $directory . "/templates/includes/header-mini.php"; ?>

        <?php /*include_once $directory . "/templates/includes/page-header.php";*/ ?>

        <?php /*include_once $directory . "/templates/includes/hero-images.php";*/ ?>

        <a id="main-content"></a>

        <div class="container-fluid uw-body">
            <div class="row">
                <?php /*include_once $directory . "/templates/includes/sidebar-left.php";*/ ?>

                <div class="uw-content <?php print $content_column_class; ?>">
                    <div class="content-header">

                        <?php print render($page['help']); ?>

                        <?php if (!$is_front && !empty($title)): ?>
                            <?php print render($title_prefix); ?>
                            <h1 class="page-title"><?php print $title; ?></h1>
                            <?php print render($title_suffix); ?>
                        <?php endif; ?>

                        <?php /*include_once $directory . "/templates/includes/content-header.php";*/ ?>
                        <?php print render($tabs); ?>

                    </div>

                    <div class="uw-body-copy" id='main_content'>

                        <?php print render($page['content_top']); ?>

                        <div class="clearfix"></div>
                        <?php print render($page['content']); ?>

                        <div class="clearfix"></div>
                        <?php print render($page['content_bottom']); ?>

                    </div>

                </div>

                <?php /*include_once $directory . "/templates/includes/sidebar-right.php";*/ ?>

            </div><!-- /#row -->
        </div><!-- /#uw-body -->

        <?php /*include_once $directory . "/templates/includes/footer.php";*/ ?>

        <?php include_once $directory . "/templates/includes/uwfooter.php"; ?>

    </div><!-- /#uw-container-inner -->
</div><!-- /#uw-container -->

