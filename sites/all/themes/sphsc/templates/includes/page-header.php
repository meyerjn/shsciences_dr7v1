<a name="top-anchor"></a>
<header class="sphsc-logos-header header">
        <nav class="container top-nav">
                <h1 class="logo col-md-4 col-sm-12">
                    <a href="<?php print $front_page; ?>" title="<?php print $site_name; ?>"><img src="/<?php print $directory . "/assets/sphsc-logo-web-1.png"; ?>"></a>
                </h1>
                <!--//logo-->
                <div class="info col-md-8 col-sm-12">
<?php
$tree = menu_tree('menu-go-menu'); 
print render($tree); 

?>
                                <?php /***** ?>
                                <ul class="go-links pull-right hidden-xs">
                                        <li><a href="index.html">Donate</a></li>
                                        <li class="divider"><a href="faq.html">Visit Clinics</a></li>
                                        <li class="divider"><a href="contact.html">Apply</a></li>
                                </ul>
                                <br>
                                <div class="contact pull-right">
                                        <p><i class="fa fa-phone"></i><a href="tel:800-123-4567">Call us today 1+ 800-123-4567</a></p>
                                        <p><i class="fa fa-envelope"></i><a href="mailto:sphsc-enquires@uw.edu">sphsc-enquires@uw.edu</a></p>
                                </div><?php *****/ ?>

                </div>
                <!--//info-->
        </nav>

        <nav class="main-nav" role="navigation">
                <div class="container">
                        <div class="navbar-header">
                                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                </button>
                                <!--//nav-toggle-->
                        </div>
                        <!--//navbar-header-->
                        <div class="navbar-collapse collapse" id="navbar-collapse">

<?php
$tree = menu_tree_output(menu_tree_all_data('main-menu')); 
print render($tree);
?>



                        </div>
                        <!--//navabr-collapse-->
                </div>
                <!--//container-->
        </nav>
        
        <div class="clearfix"></div>

</header>
