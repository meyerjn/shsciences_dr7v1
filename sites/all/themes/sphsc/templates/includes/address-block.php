<address class="sphsc-address">
        <p>Department of Speech and Hearing Sciences, University of Washington</p>
        <p class="adr-group">
                <i class="fa fa-map-marker pull-left"></i> Eagleson Hall (
                <a class="no-line" href="http://uw.edu/maps/?egl" target="new">map</a>) <span class="street-address"><br>
        1417 N.E. 42nd St.</span>
                <br> Box 354875
                <br>
                <span class="region">Seattle, WA</span>
                <span class="postal-code">98105-6246</span>
        </p>
        <p class="adr-group">
                <i class="fa fa-phone pull-left"></i> <a href="tel:206-685-7400">(206) 685-7400</a>
                <br> (206) 543-1093 fax
        </p>
        <!-- <p class="adr-group">
                <i class="fa fa-envelope pull-left"></i> <a href="mailto:sphsc-enquires@uw.edu">sphsc-enquires@uw.edu</a>
        </p> -->
        <p class="adr-group">
                <i class="fa fa-list pull-left"></i> <a href="#quick-contact-toggle" id="quick-contact-toggle">Quick Contact Form</a>
        </p>
</address>
