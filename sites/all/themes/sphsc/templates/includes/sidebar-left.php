<?php if (!empty($page['sidebar_left'])): ?>

    <aside class="<?php print $sidebar_column_class ?> col-md-3 uw-sidebar sidebar-left" role="complementary">

        <?php print render($page['sidebar_left']); ?>


    </aside>  

<?php endif; ?>
