<style type="text/css">
    .offscreen {
        position: absolute;
        left: -10000px;
        top: -10000px;
    }
</style>
<script type="text/javascript" src="http://www.trumba.com/scripts/spuds.js"></script>
<div role="region" aria-labelledby="sea_artsci">
        <h1 id="calendar_name">College of Arts and Sciences</h1>
</div>
<div role="region" aria-labelledby="main_calendar_view">
        <h2 class="offscreen" id="main_calendar_view">Main Calendar View</h2>
        <script type="text/javascript">
        var originalDocumentTitle = document.title;

        function onFetched() {
                // getEventSummary will return an object on event detail and null on calendar view.
                var eventSummary = spud.getEventSummary();
                if (eventSummary) {
                        document.title = originalDocumentTitle + ": " + eventSummary.description;
                } else {
                        document.title = originalDocumentTitle;
                }
        }
        var spudId = $Trumba.addSpud({
                webName: "sea_artsci",
                spudType: "main",
                url: {
                        headinglevel: 3
                }
        });
        var spud = $Trumba.Spuds.controller.getSpudById(spudId);
        spud.container.addEventListener("onFetched", onFetched);
        </script>
</div>




<h3>Keyword search box</h3>
<script type="text/javascript">
$Trumba.addSpud({
        webName: "sea_artsci",
        spudType: "searchlabeled",
        url: {
                headinglevel: 2
        }
});
</script>



<div role="region" aria-labelledby="calendar_display_view_selector">
        <h2 class="offscreen" id="calendar_display_view_selector">Change Calendar Display View</h2>
        <script type="text/javascript">
        $Trumba.addSpud({
                webName: "sea_artsci",
                spudType: "tabchooser",
        });
        </script>
</div>



<h3>Date finder</h3>
<script type="text/javascript">
$Trumba.addSpud({
        webName: "sea_artsci",
        spudType: "datefinder",
        url: {
                headinglevel: 2
        }
});
</script>



<h3>Event type filter</h3>
<script type="text/javascript">
$Trumba.addSpud({
        webName: "sea_artsci",
        spudType: "filter",
        spudConfig: "Type of Event",
        url: {
                headinglevel: 2
        }
});
</script>


<h3>Upcoming events Note: If you prefer to publish a short list of upcoming events, you can use the upcoming spud code:</h3>
<div role="region" aria-labelledby="upcoming_events">
        <h2 class="offscreen" id="upcoming_events">Upcoming Events</h2>
        <script type="text/javascript">
        $Trumba.addSpud({
                webName: "sea_artsci",
                spudType: "upcoming",
                url: {
                        headinglevel: 3
                },
                teaserBase: "the url of the page where the main spud is embedded"
        });
        </script>
</div>
