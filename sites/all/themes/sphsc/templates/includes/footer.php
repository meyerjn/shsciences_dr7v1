<footer class="sphsc-footer container-fluid">
        <?php print render($page['footer']); ?>

		<section id="quick-contact-form" class="row hidden">
			
			<div class="col-sm-7 col-md-offset-2 col-sm-12">
			<a id="#quick-contact-toggle"></a>
				<?php
					$block = module_invoke('webform', 'block_view', 'client-block-2244');
					print render($block['content']);

				?>
			</div>
		</section>

		<a id="top-anchor-link" class="hidden" href="#" data-toggle="Top"><i class="fa fa-angle-up"></i></a>
</footer>
