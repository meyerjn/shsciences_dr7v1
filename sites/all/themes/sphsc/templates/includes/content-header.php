<header id="content-header">

	<?php /*if (!empty($site_name)): ?>
			<a href="<?php print $front_page; ?>" title="<?php print $site_name; ?>"><h2 class="uw-site-title"><?php print $site_name; ?></h2>
					<p><?php if (!empty($site_slogan)): print $site_slogan; endif; ?></p>
			</a>
	<?php endif; */?>
			
	<?php if (!empty($breadcrumb)): ?>
			<nav class="uw-breadcrumbs" role="navigation" aria-label="breadcrumbs">
					<?php print $breadcrumb; ?>
			</nav>
	<?php endif;?>
										 
	<?php include_once $directory . "/templates/includes/mobilemenu.php"; ?>

	<?php print $messages; ?>

	<?php if (!empty($page['highlighted'])): ?>
		<div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
	<?php endif; ?>

	<?php if (!$is_front && !empty($title)): ?>
		<?php print render($title_prefix); ?>
		<h1 class="page-title"><?php print $title; ?></h1>
		<?php print render($title_suffix); ?>
	<?php endif; ?>

	<?php if (!empty($action_links)): ?>
		<ul class="action-links"><?php print render($action_links); ?></ul>
	<?php endif; ?>

	<?php if (!empty($tabs)): ?>
		<?php print render($tabs); ?>
	<?php endif; ?>

	<?php if (!empty($primarytabs)): ?>
		<?php print $primarytabs; ?>
	<?php endif; ?>



</header>