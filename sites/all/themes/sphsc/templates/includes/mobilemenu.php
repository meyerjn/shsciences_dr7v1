<?php if ((!empty($page['navigation']['system_main-menu']))): ?>  
  <nav id="mobile-relative" role="navigation" aria-label="relative">
      <button class="uw-mobile-menu-toggle">Menu</button>
      <ul class="uw-mobile-menu first-level">
          <?php if (($is_front || (!$is_front && !$uw_sidebar_menu)) && !empty($primary_nav)): ?>
            <li class="pagenav">
                <?php print l("Home", $GLOBALS['base_url'], array('attributes' => array('title' => 'Home', 'class' => array('homelink')))); ?>
                <?php print render($primary_nav); ?>
            </li><!-- /#primary_nav --> 
          <?php endif; ?>     
          <?php if (!$is_front && $uw_sidebar_menu): ?>
            <li class="pagenav">
                <?php print l("Home", $GLOBALS['base_url'], array('attributes' => array('title' => 'Home', 'class' => array('homelink')))); ?>
                <?php print render($uw_sidebar_menu); ?>
            </li><!-- /#uw_sidebar_menu --> 
          <?php endif; ?>
      </ul>
  </nav><!-- /#uw-mobile-menu -->
<?php endif; ?>