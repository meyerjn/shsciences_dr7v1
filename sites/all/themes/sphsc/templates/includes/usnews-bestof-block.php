<div id="usnews-promo">
        <h2 class="block-title">Graduate School Rankings</h2>
        <div class="pull-left">
                <img src="/sites/all/themes/sphsc/assets/us-news.png" width="125" height="117">
        </div>
        <div class="right">
                <p><a href="http://grad-schools.usnews.rankingsandreviews.com/best-graduate-schools/top-health-schools?int=grad:wid:pfbtm">Best Health Schools</a> - U.S. News and World Report ranks the top graduate, health degree programs across the nation.</p>
                <p>The Speech-Language Pathology Master of Science programs at the UW are both
                        <a href="http://grad-schools.usnews.rankingsandreviews.com/best-graduate-schools/top-health-schools/pathology-rankings?int=9cc908&int=a06908">ranked #3</a> out of 263 accredited programs.</p>
                <p>The Doctor of Audiology program (Au.D.) at the UW is <a href="http://grad-schools.usnews.rankingsandreviews.com/best-graduate-schools/top-health-schools/audiology-rankings?int=9cc908&int=a06908">ranked #3</a> out of 74 accredited programs.</p>
        </div>
</div>
