<header class="uw-hero-image" style="background-image:url('<?php print $uw_hero_image_front_path; ?>');">
    <div class="col-xs-12">

        <?php if ($is_front): ?>

            <?php if (!empty($site_name)): ?>

                <h1 style="color:<?php print $uw_front_title_color; ?>;text-shadow:<?php print $uw_front_title_text_shadow; ?>;"><?php print $site_name; ?></h1>
                <?php if (!empty($site_slogan)): ?>
                    <p style="color:<?php print $uw_front_slogan_color; ?>;text-shadow:<?php print $uw_front_slogan_text_shadow; ?>;"><?php print $site_slogan; ?></p>
                <?php endif; ?>   

            <?php endif; ?>   



        <?php else: ?>   

            <h1 class="page-title"><?php print empty($masthead_title) ? $site_name : $masthead_title; ?></h1>
            <p><?php if (!empty($site_slogan)): print $site_slogan; endif; ?></p>


        <?php endif; ?>   


    </div>
</header>

<!-- /#uw-hero-image -->