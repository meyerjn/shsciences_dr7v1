<div id="syndicate">
    <ul class="sphsc-syndicate-list">
        <li><a href="http://facebook.com"><i class="fa fa-facebook-official fa-2x"></i> Facebook</a></li>
        <li><a href="http://twitter.com"><i class="fa fa-twitter fa-2x"></i> Twitter</a></li>
        <li><a href="/news-and-events"><i class="fa fa-rss fa-2x"></i> News Feed</a></li>
        <li><a href="http://youttube.com"><i class="fa fa-youtube fa-2x"></i> YouTube</a></li>
        <li><a href="mailto:mary@google.com"><i class="fa fa-envelope-o fa-2x"></i> Mailing List</a></li>
    </ul>
</div>
