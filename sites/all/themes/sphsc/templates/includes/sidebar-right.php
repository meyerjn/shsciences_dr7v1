<?php if ( !empty($page['sidebar_first']) || !empty($page['sidebar_second']) ): ?>

    <aside class="<?php print $sidebar_column_class ?> uw-sidebar sidebar-right" role="complementary">

            <?php if ((!empty($page['navigation']['system_main-menu'])) &&  $uw_sidebar_menu): ?>
                <nav id="desktop-relative" role="navigation" aria-label="relative">
                    <ul class="uw-sidebar-menu first-level">
                        <li class="pagenav">
                            <?php print l("Home", $GLOBALS['base_url'], array('attributes' => array('title' => 'Home', 'class' => array('homelink')))); ?>
                            <?php print render($uw_sidebar_menu); ?>
                        </li>
                    </ul>
                </nav>
            <?php endif; ?>

            <?php print render($page['sidebar_first']); ?>
            <?php print render($page['sidebar_second']); ?>


    </aside>      
<?php endif; ?>

    
