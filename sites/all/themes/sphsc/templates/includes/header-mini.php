<!-- 
MyUW, Maps, Directories, Calendar, Libraries
--> 
<link rel="stylesheet" href="/<?php print $directory ?>/templates/UW-Header-Templates-master/css/mini.css">

<header class="sphscuw-thinstrip">
    <nav class="sphscuw-thin-strip-nav">
        <ul class="sphscuw-thin-links">
<!--             <li>
                <a href="/" title="Speech and Hearing Sciences">SPHSC Home</a>
            </li>
 -->
            <li>
                <a href="http://myuw.washington.edu" title="MyUW">MyUW</a>
            </li>
            <li>
                <a href="http://uw.edu/maps" title="UW Maps">Maps</a>
            </li>
            <li>
                <a href="http://uw.edu/directory" title="UW Directories">Directories</a>
            </li>
            <li>
                <a href="http://uw.edu/calendar" title="UW Calendar">Calendar</a>
            </li>
            <li>
                <a href="http://www.lib.washington.edu/" title="UW Libraries">Libraries</a>
            </li>
        </ul>
    </nav>
    <div class="container">
        <a class="sphscuw-patch" href="http://uw.edu" tabindex="-1" title="University of Washington Home">Home</a> <a class="sphscuw-wordmark" href="http://uw.edu" title="University of Washington Home">Home</a>
    </div>
</header>
