<div id="usnews-promo">
        <h2 class="block-title">Program Accreditation</h2>
        <div>
                <img src="/sites/all/themes/sphsc/assets/asha-org.png" width="200" height="73">
        </div>
        <div class="right">
                <p>The Master of Science and Doctor of Audiology programs are certified by the American Speech-Language-Hearing Association’s <a href="http://www.asha.org/academic/accreditation/">Council on Academic Accreditation in Audiology and Speech-Language Pathology (CAA)</a>.</p>
        </div>
</div>
