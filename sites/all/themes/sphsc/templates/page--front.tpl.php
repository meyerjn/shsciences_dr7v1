<div id="uw-container" class="<?php print $page_classes?>">
    <?php /*include_once $directory . "/templates/includes/quicklinks.php";*/ ?>

    <div id="uw-container-inner">
        <?php /*include_once $directory . "/templates/includes/thinstrip.php"; */?>

        <?php /* include_once $directory . "/templates/includes/dwgdrops.php"; */ ?>

        <?php include_once $directory . "/templates/includes/header-mini.php"; ?>

        <?php include_once $directory . "/templates/includes/page-header.php"; ?>

        <?php /*include_once $directory . "/templates/includes/hero-images.php";*/ ?>

        <?php print views_embed_view('hero_images', 'attachment_1'); ?>

        <a id="main-content"></a>

        <div class="container uw-body">
            <div class="row content-header">
                <div class="col-sm-12">

                    <?php print render($page['help']); ?>
                    
                    <?php include_once $directory . "/templates/includes/content-header.php"; ?>

                </div>
            </div>

            <?php if(!empty($page['content_top'])): ?>
            <div class="row content-top">
                <div class="col-sm-12">

                    <?php print render($page['content_top']); ?>

                </div>
            </div>
            <?php endif; ?>

            <div class="row">
                <div class="col-sm-12">

                    <?php print render($page['content']); ?>

                </div>
            </div>

            <?php if(!empty($page['content_bottom'])): ?>
            <div class="row content-bottom">
                <div class="col-sm-12">

                    <?php print render($page['content_bottom']); ?>

                </div>
            </div>
            <?php endif; ?>

            <div class="row">
                <div class="left-col col-sm-6 col-md-4">

                    <?php print render($page['sidebar_left']); ?>

                </div>

                <div class="col-sm-6 col-md-8">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">

                            <?php print render($page['sidebar_first']); ?>

                        </div>

                        <div class="col-xs-12 col-sm-6">

                            <?php print render($page['sidebar_second']); ?>

                        </div>
                    </div>

                </div>
            </div>
        </div><!-- /#uw-body -->

        <?php include_once $directory . "/templates/includes/footer.php"; ?>
        <?php include_once $directory . "/templates/includes/uwfooter.php"; ?>

    </div><!-- /#uw-container-inner -->
</div><!-- /#uw-container -->