(function($) {


    init = function() {

        var $wrapper = $('.hero-wrapper');

        var $slider = $('.hero-wrapper .hero-slides').bxSlider({
            touchEnabled: true,
            randomStart: false,
            auto: true, //start
            //autoHover: true, // pause on hover 
            adaptiveHeight: true,
            slideWidth: 9999,
            minSlides: 1,
            maxSlides: 1,
            moveSlides: 1,
            slideMargin: 10,
            mode: 'fade',
            speed: 900,
            onSliderLoad: function(currentIndex) {
                $wrapper.fadeIn('fast', function() {

                    $slider.redrawSlider();
                });
            }
        });


        $slider.find('.title .fa').click(function() {

            var open = $wrapper.hasClass('expanded');

            if (open) {
                $wrapper.removeClass('expanded');
                $slider.redrawSlider();
                $slider.goToNextSlide();
                $slider.startAuto();

            } else {
                $wrapper.addClass('expanded');
                $slider.redrawSlider();
                $slider.stopAuto();

                var $slide = $(this).parents('.hero-slide'),
                    n = $slide.find('img').height();

                $.each([$slide, $('.bx-viewport')], function(a, b) {
                    b.height(n);
                });

            }
        });

        // Stop after a time 
        window.setTimeout(function() {
            $slider.stopAuto();
            $slider.goToSlide(1);
            
        }, 1000 * 60 * 10);



    };


    $(document).ready(function() {

        if (Drupal.settings.sphsc.isFront) {
            init();

        }


    });


})(jQuery);
