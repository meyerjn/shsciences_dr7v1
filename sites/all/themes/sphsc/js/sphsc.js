(function($, Drupal, window, document, undefined) {


    var affixThinstrip = function() {

        var opts = {
            offset: {
                top: 100,
                bottom: function() {
                    return (this.bottom = $('footer.uw-footer').outerHeight(true) + 100)
                }
            }
        };
        
        $('header.sphscuw-thinstrip').affix(opts);
    };


    var doFrontPageIcons = function() {

        // var cal = '<i class="fa-li fa fa-calendar"></i>',
        //     news = '<i class="fa-li fa fa-newspaper-o"></i>',
        //     $ul = $('body.front .view-sphsc-articles .view-content ul'),
        //     $li = $ul.find('li');

        // $ul.addClass('fa-ul');
        // $.each($li, function(a, b) {
        //     var icon = (Math.random() < 0.5) ?
        //         cal : news;
        //     $(b).prepend(icon);
        // });

    };


    var animateFooterContactUs = function() {

        $form = $('footer #quick-contact-form');
        $link = $('a#quick-contact-toggle');
        
        $link.click(function() {

            $form.toggleClass('hidden');

        }); 

    };


    var animateSitemap = function() {

        var $sitemap = $('footer #block-footer-sitemap-footer-sitemap'),
            $toggle = $('<span><i class="fa fa-compress"></i><i class="fa fa-expand"></i></span>');

        if (!!Cookies.getJSON('sphsc') && !!Cookies.getJSON('sphsc')['hide_sitemap']) {
            $sitemap.addClass('closed');
        }

        $sitemap.addClass('expandable').append('<div class="gradient-foot">').find('h2.block-title').append($toggle);

        $sitemap.find('ul a').hover(
            function() {
                $sitemap.find('ul').removeClass('hovering');
                $(this).parents('ul').eq(0).addClass('hovering');
            },
            function() {
                $sitemap.find('ul').removeClass('hovering');
            }
        );

        $toggle.click(function() {

            var hide_sitemap = $sitemap.hasClass('closed') ? 0 : 1;
            Cookies.set('sphsc', {
                'hide_sitemap': hide_sitemap
            });
            $sitemap.toggleClass('closed', hide_sitemap);


        });

    };


    var animateTopAnchor = function() {

        var $a = $('footer a#top-anchor-link'),
            done = false;

        $(window).scroll(function() {
            if (!done && ($(this).scrollTop() > 600)) {
                $a.removeClass('hidden');
                done = true;
            }

        });

    };


    var classifyLinks = function() {

        var $a = $('#uw-container .uw-body a'),
            regExp = new RegExp("//" + location.host + "($|/)");

        $a.each(function(a, b) {

            if (b.href.charAt(0) === '#') $(b).addClass('anchor');
            if (b.href.indexOf('#') > 0 && (b.href.indexOf(':') < 0 || b.href.indexOf(location.hostname) > 0)) $(b).addClass('anchor');
            if ((b.href.substring(0, 4) === "http") && !regExp.test(b.href)) $(b).addClass('off-site');

        });

    };








    $(document).ready(function() {

        affixThinstrip();
        animateSitemap();
        animateFooterContactUs();

        if (Drupal.settings.sphsc.isFront) {
            //doFrontPageIcons();
        }

        if (!Drupal.settings.sphsc.isFront) {
            animateTopAnchor();
            classifyLinks();
        }


    });




})(jQuery, Drupal, this, this.document);


