<?php


function sphsc_preprocess_html(&$variables) { 

	global $base_url;
	
	// Adding jQuery UI effects library
	drupal_add_library('system', 'effects');
	drupal_add_css(path_to_theme() . '/library/Font-Awesome-master/css/font-awesome.min.css');
	drupal_add_js(path_to_theme() . '/library/js-cookie-master/src/js.cookie.js');
	
	
	if (drupal_is_front_page()) {
			
			drupal_add_css(path_to_theme() . '/library/jquery.bxslider/jquery.bxslider.css');
			drupal_add_js(path_to_theme() . '/library/jquery.bxslider/jquery.bxslider.js');
			drupal_add_js(path_to_theme() . '/js/sphsc-hero.js');
			
	}
	
	$jsHash = array(
			'sphsc' => array(
					'siteName' => variable_get('site_name', ''),
					'siteSlogan' => variable_get('site_slogan', ''),
					'isFront' => $variables['is_front'] ? 'true' : 'false',
					'pageTitle' => strip_tags(drupal_get_title()),
					'pathToTheme' => path_to_theme(),
					'pageURL' => $base_url . base_path() . path_to_theme()
			)
	);
	drupal_add_js($jsHash, 'setting');
	
	$variables['classes_array'][] =  'page-depth-' . (substr_count($_SERVER['REQUEST_URI'], '/') - 1); 
	$variables['classes_array'][] = 'page' . str_replace('/', '-', strtolower(dirname($_SERVER['REQUEST_URI'])));

		
}


function sphsc_preprocess_page(&$variables) { 

	$variables['directory'] = path_to_theme();
	
	$variables['content_column_class'] = 'col-md-8';
	$variables['sidebar_column_class'] = '';	

	$left = !empty($variables['page']['sidebar_left']);
	$right = !empty($variables['page']['sidebar_first']) || !empty($variables['page']['sidebar_second']);
	
	if (!empty($variables['node']->type) && $variables['node']->type == 'page') {

		$node = node_load($variables['node']->nid);
		$content = field_view_field('node', $node, 'field_sidebar_body');

		if(!empty($content[0]['#markup'])) {
			$content[0]['#markup'] = '<section class="block">'. $content[0]['#markup'] .'</section>';
			$right = $variables['page']['sidebar_second']['field_sidebar_body'] = $content; 
		}
	}

	if ($left || $right) {
		$variables['content_column_class'] = 'col-md-8';
		$variables['sidebar_column_class'] = 'col-md-4';
			
	}
	if ($left && $right) {
		$variables['content_column_class'] = 'col-md-6';
		$variables['sidebar_column_class'] = 'col-md-3';
			
	}

	if (!$left && !$right) {
		$variables['content_column_class'] = 'col-md-8 col-md-offset-2';
		
	}

	// Set a custom title over the header background 
	if (!empty($variables['node']->field_masthead_title['und'][0]['value'])) {
		$variables['masthead_title'] = $variables['node']->field_masthead_title['und'][0]['value'];
	}

	// Set a random header background image 
	// must be under public://files 
	$images = array('header-images/FallCampus_25.jpg', 
		'header-images/FallCampus_30.jpg', 
		'header-images/FallCampus_32.jpg', 
		'header-images/WinterCampus_1.jpg', 
		'header-images/WinterCampus_10.jpg',
		); 
	$image_url = file_build_uri($images[array_rand($images, 1)]); 
	$variables['uw_hero_image_front_path'] = image_style_url('banner_image', $image_url);

	// Set the background image to the node field  
	if (!empty($variables['node']->field_masthead_image['und'][0]['uri'])) {

		$image_url = $variables['node']->field_masthead_image['und'][0]['uri']; 
		$image_url = image_style_url('banner_image', $image_url);
		if(!empty($image_style_url)) {
			$variables['uw_hero_image_front_path'] = image_style_url('banner_image', $image_url);
		}
		
	}

	$variables['page_classes'] = 'page-' . end($variables['theme_hook_suggestions']); 
	reset($variables['theme_hook_suggestions']);


}



function sphsc_css_alter(&$css) { 
		// Remove defaults.css file.
		$file = drupal_get_path('module', 'footer_sitemap') . '/footer_sitemap.css';
		unset($css[$file]);
		
}




function sphsc_menu_link__main_menu($variables) {
    $element = $variables['element'];
    $sub_menu = '';
    
    // Generate as dawgdrops-item.
    $element['#attributes']['class'][] = 'dawgdrops-item';
    
    if ($element['#below']) {
    
        if ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] == 1)) {
            
            // Set dropdown trigger element to # to prevent inadvertant page loading
            // when a submenu link is clicked.
            //$element['#localized_options']['attributes']['data-target'] = '#';
            $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
            $element['#localized_options']['attributes']['aria-haspopup'][] = 'true';
            $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';

            // Add our own wrapper.
            unset($element['#below']['#theme_wrappers']);
            $sub_menu = '<ul class="dawgdrops-menu" aria-hidden="true" aria-label="submenu">' . drupal_render($element['#below']) . '</ul>';
        }
        else {
			$sub_menu = drupal_render($element['#below']);

        }
    } 
   
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";

}



function sphsc_menu_local_tasks(&$variables) { 

		$output = '<div class="btn-group pull-right sphsc-tab-tasks">' 
			. '<button class="btn btn-info btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' 
			. t('Actions') . '<span class="caret"></span></button>' 
			. '<ul class="dropdown-menu fa-ul">';
		
		if (!empty($variables['primary'])) {
				$variables['primary']['#prefix'] = '<li><a><em>' . t('Primary tabs') . '</em></a></li>';
				$output .= drupal_render($variables['primary']);
		}
		if (!empty($variables['secondary'])) {
				$variables['secondary']['#prefix'] = '<li role="separator" class="divider"></li>'
				. '<li><a><em>' . t('Secondary tabs') . '</em></a></li>';
				$output .= drupal_render($variables['secondary']);
		}
		
		$output .= '</ul>';

		if (!empty($variables['primary']) || !empty($variables['secondary'])) {
		return $output;
		}

}



function sphsc_menu_local_task($variables) { 

		$link = $variables['element']['#link'];
		$link_text = $link['title'];
		
		$icon = '';
		$icon_icons = array(
			'view' => 'fa-eye',
			'draft' => 'fa-edit',
			'edit' => 'fa-edit',
			'delete' => 'fa-trash-o',
			'revisions' => 'fa-history',
			'moderate' => 'fa-history',
			'history' => 'fa-history',
			'manage' => 'fa-cog',
			'settings' => 'fa-cog',
			'devel' => 'fa-cog',
			'config' => 'fa-cog',
			'access' => 'fa-user-plus',
			'grant' => 'fa-user-plus',
			);

		if (!empty($variables['element']['#active'])) {
				$active = '<span class="element-invisible">' . t('(active tab)') . '</span>';
				
				if (empty($link['localized_options']['html'])) {
						$link['title'] = check_plain($link['title']);
				}
				$link['localized_options']['html'] = TRUE;
				$link_text = t('!local-task-title!active', array(
					'!local-task-title' => $link['title'],
					'!active' => $active
				));
		}

		foreach($icon_icons as $key => $val) {
			if(stripos($link_text, $key) !== false && stripos($link_text, 'fa-fw') === false) {
					// $link_text = ($link_text === 'New draft') ? 'Edit and create draft' : $link_text; 
					// $link_text = ($link_text === 'Edit draft') ? 'Edit page draft' : $link_text; 
					// $link_text = ($link_text === 'View draft') ? 'View latest draft' : $link_text; 

 					$link_text = '<i class="fa fa-fw '. $val .'"></i> ' . $link_text;
					$link['localized_options'] += array('html' => true);
			}
		}

		$link = '<li' . (!empty($variables['element']['#active']) ? ' class="active"' : '') . '>'
			. l($link_text, $link['href'], $link['localized_options']) . "</li>\n";
			
		return  $link; 
}





function sphsc_views_pre_execute (&$view) {

	//sphsc($view);

}



function sphsc_preprocess_block(&$variables) {


	if(!empty($variables['elements']['#block']->module)) {

		if($variables['elements']['#block']->module === 'menu_block') {

			// $arr = drupal_get_breadcrumb(); 
			// $str = '<div>—'. $arr[ count($arr) - 1 ] .'</div>'. $variables['content'];

			$variables['content'] = str_replace(array('leaf', 'dawgdrops'), 'shs', $variables['content']); 

		}

	}

}



function sphsc($vars, $type = 'status') { 

	if (function_exists('dpm')) {
			dpm($vars);
	} else if (function_exists('_uw_boundless_dump')) {
			_uw_boundless_dump($vars, $type);
	} else {
			print '<pre>' . htmlentities(print_r($vars, true)) . '</pre>';
	}

}




